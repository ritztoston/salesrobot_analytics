import axios from 'axios';
import isEmpty from '../validations/isEmpty'

import {
    GET_MESSAGES,
    SET_LOADING_TRUE,
    SET_LOADING_FALSE,
    GET_MESSAGE_DETAILS,
    GET_MESSAGE_LIST_NEXT,
    GET_MESSAGE_DETAILS_CSV,
    SET_LOADING_CSV_TRUE,
    GET_RSS_MESSAGE_DETAILS,
    SET_LOADING_CSV_FALSE,
    GET_ERRORS
} from './types';

export const getRSSMessageDetails = (account, id) => dispatch => {
    dispatch(setLoadingTrue());

    axios.get(`http://www.analyticsapi.salesrobot.com/${account}/messages/rss/${id}/`)
        .then(res => {
            dispatch(getRSSMessageDetail(res));
            dispatch(setLoadingFalse())
        })
        .catch(err => dispatch({
            type: GET_ERRORS,
            payload: err.response.data
        }));
};

export const getAllMessages = account => dispatch => {
    dispatch(setLoadingTrue());

    axios.get(`http://www.analyticsapi.salesrobot.com/${account}/messages/`)
        .then(res => {
            dispatch(getMessages(res));
            dispatch(setLoadingFalse())
        })
        .catch(err => dispatch({
            type: GET_ERRORS,
            payload: err.response.data
        }));
};

export const getMessageNextList = (pageNumber, account) => dispatch => {
    dispatch(setLoadingTrue());

    axios.get(`http://www.analyticsapi.salesrobot.com/${account}/messages/?page=${pageNumber}`)
        .then(res => {
            dispatch(getMessagesList(res));
            dispatch(setLoadingFalse())
        })
        .catch(err => dispatch({
            type: GET_ERRORS,
            payload: err.response.data
        }));
};

export const getMessageDetails = (id, account, filter) => dispatch => {
    dispatch(setLoadingTrue());
    axios.get(`http://www.analyticsapi.salesrobot.com/${account}/messages/${id}/?filter=${filter}`)
        .then(res => {
            res.data.users.results.map(user => {
                if(!isEmpty(user.linktrackuml)) {
                    user.linktrackuml = JSON.parse(user.linktrackuml)
                }
            });

            dispatch(getMessageDetailsFunc(res));
            dispatch(setLoadingFalse())
        })
        .catch(err => dispatch({
            type: GET_ERRORS,
            payload: err.response.data
        }));
};

export const getMessageDetailsAll = (id, account, filter) => dispatch => {
    dispatch(setLoadingCSVTrue());
    axios.get(`http://www.analyticsapi.salesrobot.com/${account}/messages/${id}/?filter=${filter}`)
        .then(res => {
            res.data.users.map(user => {
                user.linktrackuml = JSON.parse(user.linktrackuml)
            });
            dispatch(getMessageDetailsCSV(res));
            dispatch(setLoadingCSVFalse())
        })
        .catch(err => dispatch({
            type: GET_ERRORS,
            payload: err.response.data
        }));
};

export const getMessageNextDetails = (id, pageNumber, account, filter) => dispatch => {
    dispatch(setLoadingTrue());
    axios.get(`http://www.analyticsapi.salesrobot.com/${account}/messages/${id}/`, {params: {filter: filter, page: pageNumber}})
        .then(res => {
            res.data.users.results.map(user => {
                if(!isEmpty(user.linktrackuml)) {
                    user.linktrackuml = JSON.parse(user.linktrackuml)
                }
            });
            dispatch(getMessageDetailsFunc(res));
            dispatch(setLoadingFalse())
        })
        .catch(err => dispatch({
            type: GET_ERRORS,
            payload: err.response.data
        }));
};

export const getMessages = res => {
    return {
        type: GET_MESSAGES,
        payload: res.data
    }
};

export const getRSSMessageDetail = res => {
    return {
        type: GET_RSS_MESSAGE_DETAILS,
        payload: res.data
    }
};

export const setLoadingTrue = () => {
    return {
        type: SET_LOADING_TRUE
    }
};

export const setLoadingFalse = () => {
    return {
        type: SET_LOADING_FALSE
    }
};

export const getMessageDetailsFunc = res => {
    return {
        type: GET_MESSAGE_DETAILS,
        payload: res.data
    }
};

export const getMessageDetailsCSV = res => {
    return {
        type: GET_MESSAGE_DETAILS_CSV,
        payload: res.data
    }
};

export const getMessagesList = res => {
    return {
        type: GET_MESSAGE_LIST_NEXT,
        payload: res.data
    }
};

export const setLoadingCSVTrue = () => {
    return {
        type: SET_LOADING_CSV_TRUE
    }
};

export const setLoadingCSVFalse = () => {
    return {
        type: SET_LOADING_CSV_FALSE
    }
};
