import axios from 'axios';

import {
  GET_TEMPLATE,
  SET_LOADING_TRUE,
  SET_LOADING_FALSE,
  GET_ERRORS
} from './types';

export const getAccountTemplate = () => dispatch => {
  dispatch(setLoadingTrue());

  axios.get('http://127.0.0.1:8000/poc/template/')
      .then(res => {
        dispatch(setTemplate(res));
        dispatch(setLoadingFalse())
      })
      .catch(err => dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      }));
};

export const setTemplate = res => {
  return {
    type: GET_TEMPLATE,
    payload: res.data
  }
};

export const setLoadingTrue = () => {
  return {
    type: SET_LOADING_TRUE
  }
};

export const setLoadingFalse = () => {
  return {
    type: SET_LOADING_FALSE
  }
};
