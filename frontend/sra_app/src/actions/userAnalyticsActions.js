import axios from 'axios';

import {
  GET_USER_ANALYTICS,
  GET_USER_ANALYTICS_DETAIL,
  SET_LOADING_TRUE,
  SET_LOADING_FALSE,
  GET_USER_LIST_NEXT,
  GET_USER_LIST_SEARCH,
  GET_ERRORS
} from './types';

export const getUserAnalytics = () => dispatch => {
  dispatch(setLoadingTrue());

  axios.get('http://www.analyticsapi.salesrobot.com/public/subscribers/')
      .then(res => {
        dispatch(getUserAl(res));
        dispatch(setLoadingFalse())
      })
      .catch(err => dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      }));
};

export const getUserAnalyticsSearch = email => dispatch => {
  dispatch(setLoadingTrue());

  axios.get(`http://www.analyticsapi.salesrobot.com/public/subscribers/?email=${email}`)
      .then(res => {
        dispatch(getUserSearch(res));
        dispatch(setLoadingFalse())
      })
      .catch(err => dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      }));
};

export const getUserNextList = (pageNumber) => dispatch => {
  dispatch(setLoadingTrue());

  axios.get(`http://www.analyticsapi.salesrobot.com/public/subscribers/?page=${pageNumber}`)
      .then(res => {
        dispatch(getUserList(res));
        dispatch(setLoadingFalse())
      })
      .catch(err => dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      }));
};

export const getAllUserAnalyticsDetail = email => dispatch => {
  dispatch(setLoadingTrue());

  axios.get(`http://www.analyticsapi.salesrobot.com/public/subscribers/all/${email}/`)
      .then(res => {
        dispatch(getAllUserDetail(res));
        dispatch(setLoadingFalse())
      })
      .catch(err => dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      }));
};

export const getUserAnalyticsDetail = id => dispatch => {
  dispatch(setLoadingTrue());

  axios.get(`http://www.analyticsapi.salesrobot.com/poc/subscribers/${id}/`)
      .then(res => {
        dispatch(getUserDetail(res));
        dispatch(setLoadingFalse())
      })
      .catch(err => dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      }));
};

export const getUserAl = res => {
  return {
    type: GET_USER_ANALYTICS,
    payload: res.data
  }
};

export const getUserDetail = res => {
  return {
    type: GET_USER_ANALYTICS_DETAIL,
    payload: res.data
  }
};

export const getAllUserDetail = res => {
  return {
    type: GET_USER_ANALYTICS_DETAIL,
    payload: res.data[0]
  }
};

export const getUserSearch = res => {
  return {
    type: GET_USER_LIST_SEARCH,
    payload: res.data
  }
};

export const getUserList = res => {
  return {
    type: GET_USER_LIST_NEXT,
    payload: res.data
  }
};

export const setLoadingTrue = () => {
  return {
    type: SET_LOADING_TRUE
  }
};

export const setLoadingFalse = () => {
  return {
    type: SET_LOADING_FALSE
  }
};
