import {GET_MESSAGE_DETAILS_CSV, SET_LOADING_CSV_TRUE, SET_LOADING_CSV_FALSE} from '../actions/types';

const initialState = {
    loading: false,
    data: {
        users: []
    }
};

export default function(state = initialState, action){
    switch (action.type) {
        case GET_MESSAGE_DETAILS_CSV:
            return {
                ...state,
                data: action.payload
            };
        case SET_LOADING_CSV_TRUE:
            return {
                ...state,
                loading: true
            };
        case SET_LOADING_CSV_FALSE:
            return {
                ...state,
                loading: false
            };
        default:
            return state;
    }
}
