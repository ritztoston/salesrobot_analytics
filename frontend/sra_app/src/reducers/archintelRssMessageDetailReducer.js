import {GET_RSS_MESSAGE_DETAILS} from '../actions/types';

const initialState = [{
    itemid: 0,
    clicked: 0,
    title: '',
    url: '',
    category: '',
    image: '' }
];

export default function(state = initialState, action){
    switch (action.type) {
        case GET_RSS_MESSAGE_DETAILS:
            return action.payload;
        default:
            return state;
    }
}
