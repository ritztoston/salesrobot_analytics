import {GET_USER_ANALYTICS_DETAIL, CLEAR_USER_ANALYTICS_DETAIL} from '../actions/types';

const initialState = {
    email: '',
    confirmed: 0,
    blacklisted:0,
    bouncecount: 0,
    linktrackuml: []
};

export default function(state = initialState, action){
    switch (action.type) {
        case GET_USER_ANALYTICS_DETAIL:
            return action.payload;
        case CLEAR_USER_ANALYTICS_DETAIL:
            return [];
        default:
            return state;
    }
}
