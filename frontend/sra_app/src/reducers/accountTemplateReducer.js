import {GET_TEMPLATE} from '../actions/types';

const initialState = [];

export default function(state = initialState, action){
    switch (action.type) {
        case GET_TEMPLATE:
            return action.payload;
        default:
            return state;
    }
}
