import {GET_MESSAGES, CLEAR_ALL_MESSAGES, GET_MESSAGE_LIST_NEXT} from '../actions/types';

const initialState = {
    count: 0,
    next: null,
    previous: null,
    results: []
};

export default function(state = initialState, action){
    switch (action.type) {
        case GET_MESSAGES:
            return action.payload;
        case GET_MESSAGE_LIST_NEXT:
            return action.payload;
        case CLEAR_ALL_MESSAGES:
            return {};
        default:
            return state;
    }
}
