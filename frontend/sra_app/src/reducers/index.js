import {combineReducers} from 'redux';
import authReducer from './authReducer';
import errorReducer from './errorReducer';
import verifyReducer from './verifyReducer';
import userReducer from './userReducer';
import userAnalyticsReducer from './userAnalyticsReducer';
import userAnalyticsDetailReducer from './userAnalyticsDetailReducer';
import messagesReducer from './messagesReducer';
import messageDetailsReducer from './messageDetailsReducer';
import messageDetailsCSVReducer from './messageDetailsCSVReducer';
import archintelAccountReducer from './archintelAccountReducer';
import accountTemplateReducer from './accountTemplateReducer';
import archintelRssMessageDetailReducer from './archintelRssMessageDetailReducer';

export default combineReducers({
    auth: authReducer,
    errors: errorReducer,
    verification: verifyReducer,
    users: userReducer,
    userAnalytics: userAnalyticsReducer,
    userAnalyticsDetail: userAnalyticsDetailReducer,
    messages: messagesReducer,
    messageDetails: messageDetailsReducer,
    messageDetailsCSV: messageDetailsCSVReducer,
    archintel_accounts: archintelAccountReducer,
    account_template: accountTemplateReducer,
    rss_message_details: archintelRssMessageDetailReducer
});
