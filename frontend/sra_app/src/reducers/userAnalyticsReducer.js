import {GET_USER_ANALYTICS, CLEAR_USER_ANALYTICS, GET_USER_LIST_NEXT, GET_USER_LIST_SEARCH} from '../actions/types';

const initialState = {
    count: 0,
    next: null,
    previous: null,
    results: []
};

export default function(state = initialState, action){
    switch (action.type) {
        case GET_USER_ANALYTICS:
            return action.payload;
        case GET_USER_LIST_NEXT:
            return action.payload;
        case GET_USER_LIST_SEARCH:
            return action.payload;
        case CLEAR_USER_ANALYTICS:
            return {};
        default:
            return state;
    }
}
