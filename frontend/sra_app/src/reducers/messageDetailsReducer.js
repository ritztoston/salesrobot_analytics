import {GET_MESSAGE_DETAILS, GET_MESSAGE_NEXT_DETAILS} from '../actions/types';

const initialState = {
    users: {
        count: 0,
        next: null,
        previous: null,
        results: []
    }
};

export default function(state = initialState, action){
    switch (action.type) {
        case GET_MESSAGE_DETAILS:
            return action.payload;
        case GET_MESSAGE_NEXT_DETAILS:
            return action.payload;
        default:
            return state;
    }
}
