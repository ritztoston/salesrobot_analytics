import {GET_VERICATION_PAYLOAD} from '../actions/types';

const initialState = {};

export default function(state = initialState, action){
    switch (action.type) {
        case GET_VERICATION_PAYLOAD:
            return action.payload;
        default:
            return state;
    }
}
