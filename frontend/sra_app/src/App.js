import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import {Provider} from 'react-redux';
import jwt_decode from 'jwt-decode';
import setAuthToken from './utils/setAuthToken';
import {setCurrentUser, logoutUser} from './actions/authActions';
import Cookies from 'universal-cookie';
import store from './store';
import './App.css';

import PrivateRoute from './components/common/PrivateRoute';
import IsAuthenticatedRoute from './components/common/IsAuthenticatedRoute';

import Navbar from './components/layout/Navbar';
import Login from './components/auth/Login';
import Dashboard from './components/dashboard/Dashboard';
import EditProfile from './components/edit-profile/EditProfile';
import ForgotPassword from './components/forgot-password/ForgotPassword';
import ForgotPasswordCP from './components/forgot-password/ForgotPasswordCP';
import Users from './components/users/Users';
import Archintel from './components/archintel/Archintel';
import ArchintelAccountDetail from './components/archintel/ArchintelAccountDetail';
import ArchintelAccountDetails from './components/archintel/ArchintelAccountDetails';
import RssPreviewDetail from './components/rsspreview/RssPreviewDetail';
import UserAnalytics from './components/users/UserAnalytics';
import UserAnalyticsDetail from './components/users/UserAnalyticsDetail';
import Messages from './components/messages/Messages';
import MessagesDetail from './components/messages/MessagesDetail';
import DivWrapper from './components/hoc/DivWrapper';

// Check token from cookie
const cookie = new Cookies();
const cookie_token = cookie.get('sra_token');

if(cookie_token) {
    setAuthToken(cookie_token);
    const decoded = jwt_decode(cookie_token);
    store.dispatch(setCurrentUser(decoded));

    const message_content = "Session expired. Please login again.";
    const currentime = Date.now() /1000;
    if (decoded.exp < currentime) {
        store.dispatch(logoutUser(message_content));
        window.location.href = '/login';
    }
}

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <Router>
                    <div>
                        <Route exact path="" component={Login} />
                        <Route exact path="/login" component={Login} />
                        <Route exact path="/subscribe" component={Login} />
                        <Route exact path="/forgot-password" component={ForgotPassword} />
                        <Route exact path="/forgot-password/:username/:verify_pin" component={ForgotPasswordCP} />

                        <DivWrapper>
                            <IsAuthenticatedRoute component={Navbar} />
                            <Switch>
                                <PrivateRoute exact path="/dashboard" component={Dashboard} />
                            </Switch>
                            <Switch>
                                <PrivateRoute exact path="/edit-profile" component={EditProfile} />
                            </Switch>
                            <Switch>
                                <PrivateRoute exact path="/users" component={Users} />
                            </Switch>
                            <Switch>
                                <PrivateRoute exact path="/users-analytics" component={UserAnalytics} />
                            </Switch>
                            <Switch>
                                <PrivateRoute exact path="/users-analytics/:id/:email" component={UserAnalyticsDetail} />
                            </Switch>
                            <Switch>
                                <PrivateRoute exact path="/users-analytics/:email" component={UserAnalyticsDetail} />
                            </Switch>
                            <Switch>
                                <PrivateRoute exact path="/messages" component={Messages} />
                            </Switch>
                            <Switch>
                                <PrivateRoute exact path="/messages/:account/:id" component={MessagesDetail} />
                            </Switch>
                            <Switch>
                                <PrivateRoute exact path="/archintel" component={Archintel} />
                            </Switch>
                            <Switch>
                                <PrivateRoute exact path="/archintel/:account" component={ArchintelAccountDetail} />
                            </Switch>
                            <Switch>
                                <PrivateRoute exact path="/archintel/:account/messages/:id" component={ArchintelAccountDetails} />
                            </Switch>
                            <Switch>
                                <PrivateRoute exact path="/rss-preview/:id/:account" component={RssPreviewDetail} />
                            </Switch>
                        </DivWrapper>
                    </div>
                </Router>
            </Provider>
        );
    }
}

export default App;
