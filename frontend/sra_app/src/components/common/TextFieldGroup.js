import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

const TextFieldGroup = ({
                            name,
                            value,
                            placeholder,
                            placeholder_info,
                            label,
                            error,
                            info,
                            type,
                            onChange,
                            disabled
                        }) => {
    return (
        <div className="form__group">
            <input type={type} className={classnames('form__input', {'form__input--is-invalid': error})} placeholder={placeholder} id={name} name={name} maxLength="50" value={value} onChange={onChange} disabled={disabled}/>
            {error ? (<label htmlFor="email" className={classnames('form__invalid', {'form__invalid-show': error})}>{error}</label>) : (<label htmlFor="email" className="form__label">{placeholder_info}</label>)}
        </div>
    )
};

TextFieldGroup.propTypes = {
    name: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    placeholder_info: PropTypes.string,
    value: PropTypes.string.isRequired,
    error: PropTypes.array,
    type: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    disabled: PropTypes.string,
};

TextFieldGroup.defaultProps = {
    type: 'text'
};

export default TextFieldGroup
