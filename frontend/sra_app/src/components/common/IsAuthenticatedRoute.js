import React from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

const IsAuthenticatedRoute = ({component: Component, auth, ...rest}) => (
    auth.isAuthenticated ? (<Component {...rest}/>) : null
);

IsAuthenticatedRoute.propTypes = {
    auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(mapStateToProps)(IsAuthenticatedRoute)
