import React from 'react';
import PropTypes from 'prop-types';
import ReactAux from '../hoc/ReactAux';

const SearchTextFieldGroup = ({
                                  name,
                                  value,
                                  placeholder,
                                  placeholder_info,
                                  label,
                                  info,
                                  type,
                                  onChange,
                                  disabled
                              }) => {
    return (
        <ReactAux>
            <input type={type} className='form__input-search u-margin-bottom-l' placeholder={placeholder} id={name} name={name} maxLength="50" value={value} onChange={onChange} disabled={disabled}/>
        </ReactAux>
    )
};

SearchTextFieldGroup.propTypes = {
    name: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    value: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    disabled: PropTypes.string,
};

SearchTextFieldGroup.defaultProps = {
    type: 'text'
};

export default SearchTextFieldGroup
