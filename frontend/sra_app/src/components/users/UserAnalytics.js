import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getUserAnalytics, getUserNextList, getUserAnalyticsSearch} from '../../actions/userAnalyticsActions';
import Pace from 'react-pace-progress';
import {withRouter} from 'react-router-dom';
import "react-datepicker/dist/react-datepicker.css";
import Pagination from "react-js-pagination";
import SearchTextFieldGroup from '../common/SearchTextFieldGroup';

class UserAnalytics extends Component {
    constructor(props) {
        super(props);
        this.state = {
            startDate: new Date(),
            pagination_page: 15,
            current_page: 1,
            search_email: '',
            typing: false,
            typingTimeout: 0
        };
        this.linkClick = this.linkClick.bind(this);
        this.handleChangeDate = this.handleChangeDate.bind(this);
        this.handlePageChange = this.handlePageChange.bind(this);
        this.onChangeSearch = this.onChangeSearch.bind(this);
    }

    componentDidMount() {
        // if(results === undefined || results.length < 1)
        this.props.getUserAnalytics()
    }

    onChangeSearch(e) {
        const search_value = e.target.value;

        if (this.state.typingTimeout) clearTimeout(this.state.typingTimeout);

        this.setState({
            search_email: search_value,
            typing: false,
            typingTimeout: setTimeout(() => {
                this.doSearch(search_value)
            }, 500)
        });
    }

    doSearch(value) {
        this.props.getUserAnalyticsSearch(value)
    }

    handlePageChange(pageNumber) {
        this.props.getUserNextList(pageNumber);
        this.setState({current_page: pageNumber});
    }

    handleChangeDate(date) {
        this.setState({
            startDate: date
        });
    }

    linkClick(email) {
        this.props.history.push(`/users-analytics/${email}`);
    }

    render() {
        const {results, count} = this.props.userAnalytics;
        const {loading} = this.props.auth;
        const {current_page, pagination_page, search_email} = this.state;

        const usersAnalytics_content = results.map(result => (
            <tr onClick={() => this.linkClick(result.email, result.id)} key={result.id} className="custom-tr">
                <td>
                    <div class="d-flex align-items-center"><h5>{result.id}</h5></div>
                </td>
                <td>{result.email}</td>
                <td>{result.count === 0 ? (<span className="u-error-text">0</span>) : (<span className="u-success-text"><h5>{result.count}</h5></span>)}</td>
            </tr>
        ));

        const umain_content = (
            <div class="page-wrapper">
                <div class="page-breadcrumb">
                    <div class="row align-items-center">
                        <div class="col-5">
                            <h4 class="page-title">Subscribers</h4>
                            <div class="d-flex align-items-center">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item active u-bold-text" aria-current="page">Subscribers</li>
                                        <li class="breadcrumb-item active" aria-current="page">Subscriber Details</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="d-md-flex align-items-center">
                                        <div>
                                            <h4 class="card-title">Subsriber Details</h4>
                                            <h5 class="card-subtitle">Overview of Subscribers</h5>
                                        </div>
                                        <div class="ml-auto">
                                            <div class="dl">
                                                <div class="container-fluid">
                                                    <div class="row">
                                                        <div class="col-auto">
                                                            <h5 className="u-padding-top-6">Search Email:</h5>
                                                        </div>
                                                        <div class="col-4">
                                                            <SearchTextFieldGroup
                                                                placeholder="Email"
                                                                placeholder_info="Email"
                                                                type="text"
                                                                value={search_email}
                                                                onChange={this.onChangeSearch}
                                                                name="email"
                                                            />
                                                        </div>
                                                        <div class="col-auto">
                                                            <h5 className="u-padding-top-6">Timezone:</h5>
                                                        </div>
                                                        <div class="col-auto">
                                                            <select class="custom-select" onChange={this.selectedTimezone}>
                                                                <option value="0">EST</option>
                                                                <option value="1">PST</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table v-middle">
                                        <thead>
                                        <tr class="bg-light">
                                            <th class="border-top-0">ID</th>
                                            <th class="border-top-0">Email</th>
                                            <th class="border-top-0">Total Links Clicked</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {usersAnalytics_content}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12 u-text-align-center">
                            <div className="pagination-container">
                                <Pagination
                                    activePage={current_page}
                                    itemsCountPerPage={pagination_page}
                                    totalItemsCount={count}
                                    pageRangeDisplayed={5}
                                    onChange={this.handlePageChange}
                                    activeLinkClass="active-link"
                                    hideDisabled={true}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );

        return (
            <div>
                {loading ? <Pace color="#FA7921" height={4}/> : umain_content}
            </div>
        )
    }
}

UserAnalytics.propTypes = {
    getUserAnalytics: PropTypes.func.isRequired,
    getUserNextList: PropTypes.func.isRequired,
    getUserAnalyticsSearch: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    users: PropTypes.array.isRequired,
    userAnalytics: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
    auth: state.auth,
    users: state.users,
    userAnalytics: state.userAnalytics,
    errors: state.errors
});


export default connect(mapStateToProps, {getUserAnalytics, getUserNextList, getUserAnalyticsSearch})(withRouter(UserAnalytics));
