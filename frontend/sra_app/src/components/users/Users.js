import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {unloadMessage, userList} from '../../actions/authActions';
import Pace from 'react-pace-progress';

class Users extends Component {

    componentDidMount() {
        const {users} = this.props;
        if(users === undefined || users.length < 1)
            this.props.userList()
    }

    render() {
        const {users} = this.props;
        const {loading} = this.props.auth;
        let user_status;

        const profile_content = users.map(user => {
            if (user.staff_user < 2) {
                user_status = 'AI ';
                if (user.is_superuser) {
                    user_status += 'Admin';
                    user_status = (<span className="label u-label-error u-bold-text ">{user_status}</span>)
                }
                else {
                    user_status += 'User';
                    user_status = (<span className="label u-label-success u-bold-text ">{user_status}</span>)
                }
            } else if (user.staff_user < 4) {
                user_status = 'SOSi ';
                if (user.is_superuser) {
                    user_status += 'Admin';
                    user_status = (<span className="label u-label-error u-bold-text ">{user_status}</span>)
                }
                else {
                    user_status += 'User';
                    user_status = (<span className="label u-label-success u-bold-text ">{user_status}</span>)
                }
            }
            return (
                <tr key={user.id} className="custom-tr">
                    <td>
                        <div class="d-flex align-items-center">
                            <img className="rounded-circle u-border-m" src={user.avatar} alt="user-img" title={user.username} style={{maxWidth: "25px", width: "100%"}}/>
                            <div class="">
                                <h4 class="m-b-0 font-16 u-margin-left-s">{user.username}</h4>
                            </div>
                        </div>
                    </td>
                    <td>{user.email}</td>
                    <td>{user.firstname}</td>
                    <td>{user.lastname}</td>
                    <td>{user_status}</td>
                </tr>
            )});

        const user_content = (
            <div class="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="d-md-flex align-items-center">
                                        <div>
                                            <h4 class="card-title">User Details</h4>
                                            <h5 class="card-subtitle">Overview of Users</h5>
                                        </div>
                                        <div class="ml-auto">
                                            <div class="dl">
                                                <select class="custom-select">
                                                    <option value="0">Monthly</option>
                                                    <option value="1">Daily</option>
                                                    <option value="2">Weekly</option>
                                                    <option value="3">Yearly</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table v-middle">
                                        <thead>
                                        <tr class="bg-light">
                                            <th class="border-top-0">Username</th>
                                            <th class="border-top-0">Email</th>
                                            <th class="border-top-0">First Name</th>
                                            <th class="border-top-0">Last Name</th>
                                            <th class="border-top-0">Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {profile_content}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
        return (
            <div>
                {loading ? <Pace color="#FA7921" height={4}/> : user_content}
            </div>
        )
    }
}

Users.propTypes = {
    userList: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    users: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth,
    users: state.users,
    errors: state.errors
});


export default connect(mapStateToProps, {userList, unloadMessage})(Users);
