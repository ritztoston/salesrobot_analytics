import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getAllUserAnalyticsDetail} from '../../actions/userAnalyticsActions';
import Pace from 'react-pace-progress';
import isEmpty from '../../validations/isEmpty';
import {Link, withRouter} from 'react-router-dom';
import Moment from 'react-moment';
import 'moment-timezone';

class UserAnalyticsDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            timezone: 'America/New_York'
        };

        this.selectedTimezone = this.selectedTimezone.bind(this);
    }

    componentDidMount() {
        if(this.props.match.params.email) {
            this.props.getAllUserAnalyticsDetail(this.props.match.params.email)
        }
    }

    selectedTimezone(e) {
        this.setState({timezone: e.target.value})
    }

    render() {
        const {email, linktrackuml} = this.props.userAnalyticsDetail;
        const {loading} = this.props.auth;
        const {timezone} = this.state;

        const usersAnalytics_content = linktrackuml.map(linktrackuml => {
                const date = <Moment format="ddd MMM DD, YYYY h:mm A" add={{ hours: 5 }} tz={timezone}>{linktrackuml.clicked}</Moment>;

                return (
                    <tr key={linktrackuml.id} className="custom-tr">
                        <td>{linktrackuml.url}</td>
                        <td>{linktrackuml.clicks}</td>
                        <td className="u-bold-text">{date}</td>
                    </tr>
                )
            }
        );

        const table_content = (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-12">
                        <div className="card">
                            <div className="card-body">
                                <div className="d-md-flex align-items-center">
                                    <div>
                                        <h4 className="card-title">Subscriber Details</h4>
                                        <h5 className="card-subtitle">Overview of Subscribers</h5>
                                    </div>
                                    <div className="ml-auto">
                                        <div className="dl">
                                            <div className="container-fluid">
                                                <div className="row">
                                                    <div className="col-auto">
                                                        <h5 className="u-padding-top-6">Timezone:</h5>
                                                    </div>
                                                    <div className="col-auto">
                                                        <select className="custom-select" onChange={this.selectedCampaign}>
                                                            <option value="0">EST</option>
                                                            <option value="1">PST</option>
                                                        </select>
                                                    </div>
                                                    <div className="col-auto">
                                                        <h5 className="u-padding-top-6">Timezone:</h5>
                                                    </div>
                                                    <div className="col-auto">
                                                        <select className="custom-select" value={timezone} onChange={this.selectedTimezone}>
                                                            <option value="America/New_York">EST</option>
                                                            <option value="Asia/Manila">PST</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="table-responsive">
                                <table className="table v-middle">
                                    <thead>
                                    <tr className="bg-light">
                                        <th className="border-top-0">URL</th>
                                        <th className="border-top-0">Clicks</th>
                                        <th className="border-top-0">Clicked On</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {usersAnalytics_content}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );

        const umain_content = (
            <div className="page-wrapper">
                <div className="page-breadcrumb">
                    <div className="row align-items-center">
                        <div className="col-5">
                            <h4 className="page-title">Subscriber Details</h4>
                            <div className="d-flex align-items-center">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item"><Link to="/users-analytics">Subscribers</Link></li>
                                        <li className="breadcrumb-item active u-bold-text" aria-current="page">{email}</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                {!isEmpty(linktrackuml) ? table_content : (
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-12">
                                <h4 className="card-title">- No click activity -</h4>
                            </div>
                        </div>
                    </div>
                )}
            </div>
        );

        return (
            <div>
                {loading ? <Pace color="#FA7921" height={4}/> : umain_content}
            </div>
        )
    }
}

UserAnalyticsDetail.propTypes = {
    getAllUserAnalyticsDetail: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    users: PropTypes.array.isRequired,
    userAnalyticsDetail: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
    auth: state.auth,
    users: state.users,
    userAnalyticsDetail: state.userAnalyticsDetail,
    errors: state.errors
});


export default connect(mapStateToProps, {getAllUserAnalyticsDetail})(withRouter(UserAnalyticsDetail));
