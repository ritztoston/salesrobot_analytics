import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {loginUser, unloadMessage} from '../../actions/authActions';
import Pace from 'react-pace-progress';
import TextFieldGroup from '../common/TextFieldGroup';
import isEmpty from '../../validations/isEmpty';
import {Link} from 'react-router-dom';
import classnames from 'classnames';


class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: ''
        };

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    static getDerivedStateFromProps(nextProps) {
        if(nextProps.auth.isAuthenticated) {
            nextProps.unloadMessage();
            nextProps.history.push('/dashboard');
        }

        return null
    }

    onChange(e) {
        if(e.target.name !== 'avatar'){
            this.setState({[e.target.name]: e.target.value});
        }
        else {
            this.setState({[e.target.name]: e.target.files[0]})
        }
    }

    onSubmit(e) {
        e.preventDefault();

        const newUser = {
            username: this.state.username,
            password: this.state.password
        };

        this.props.loginUser(newUser, this.props.errors);
    }

    componentDidMount() {
        if(!isEmpty(this.props.auth.message)) {
            setTimeout(() => {
                this.props.unloadMessage()
            }, 8000);
        }
    }

    render() {
        const {errors} = this.props;
        const {loading, message} = this.props.auth;
        const message_content = (
            <div className="row u-padding-top-lg-text">
                <div className="col u-text-align-center">
                    <span className="auth__message auth__message-success">{message}</span>
                </div>
            </div>
        );
        return (
            <div className="u-bg-main">
                {loading ? <Pace color="#FA7921" height={4}/> : null}
                <div className="container">
                    {message ? message_content : null}
                </div>
                <div className="registration__form">
                    <form action="#" className="form" autoComplete="off" onSubmit={this.onSubmit} encType="application/json">
                        <div className="container">
                            <div className="row u-padding-top-lg">
                                <div className="col">
                                    <div className="form__group">
                                        <div className="u-text-align-center">
                                            <img src="http://127.0.0.1:8000/media/default/headernavblue.png" className="header--logo" alt="SalesRobot Logo" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="row u-padding-top-lg-text">
                                <div className="col">
                                    <TextFieldGroup
                                        placeholder="Username"
                                        placeholder_info="Username"
                                        type="text"
                                        value={this.state.username}
                                        onChange={this.onChange}
                                        error={errors.username}
                                        name="username"
                                    />
                                </div>
                            </div>

                            <div className="row">
                                <div className="col">
                                    <TextFieldGroup
                                        placeholder="Password"
                                        placeholder_info="Password"
                                        type="password"
                                        value={this.state.password}
                                        onChange={this.onChange}
                                        error={errors.password}
                                        name="password"
                                    />
                                </div>
                            </div>

                            <div className="row u-padding-top-lg">
                                <div className="col">
                                    <div className="form__group">
                                        <div className="u-text-align-center">
                                            <Link to="/forgot-password" className="btn__ btn__forgot">Forgot Password?</Link>
                                            <button className="btn__ btn__submit" type="submit" disabled={loading}>Login <div className={classnames('ld ld-spin', {'ld-ring': loading})}/></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

Login.propTypes = {
    loginUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
});

export default connect(mapStateToProps, {loginUser, unloadMessage})(Login);
