import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import classnames from 'classnames';
import {connect} from 'react-redux';
import {registerUser} from '../../actions/authActions';
import PropTypes from 'prop-types';

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            username: '',
            firstname: '',
            lastname: '',
            avatar: '',
            password: '',
            password2: '',
            errors: {}
        };

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    static getDerivedStateFromProps(nextProps,) {
        if(nextProps.errors) {
            return({errors: nextProps.errors})
        }
    }

    onChange(e) {
        if(e.target.name !== 'avatar'){
            this.setState({[e.target.name]: e.target.value});
        }
        else {
            this.setState({[e.target.name]: e.target.files[0]})
        }
    }

    onSubmit(e) {
        e.preventDefault();

        const newUser = {
            email: this.state.email,
            username: this.state.username,
            firstname: this.state.firstname,
            lastname: this.state.lastname,
            password: this.state.password,
            password2: this.state.password2,
            avatar: this.state.avatar
        };

        this.props.registerUser(newUser, this.props.history);
    }

    render() {
        const {errors} = this.state;

        return (
            <div className="registration__form">
                <form action="#" className="form" autoComplete="off" onSubmit={this.onSubmit} encType="application/json">
                    <div className="container">
                        <div className="row">
                            <div className="col">
                                <div className="form__group">
                                    <input type="text" className={classnames('form__input', {'form__input--is-invalid': errors.email})} placeholder="Email" id="email" name="email" value={this.state.email} onChange={this.onChange}/>
                                    {errors.email ? (<label htmlFor="email" className={classnames('form__invalid', {'form__invalid-show': errors.email})}>{errors.email}</label>) : (<label htmlFor="email" className="form__label">What is your email?</label>)}
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <div className="form__group">
                                    <input type="text" className={classnames('form__input', {'form__input--is-invalid': errors.username})} placeholder="Username" id="username" name="username" maxLength="11" value={this.state.username} onChange={this.onChange}/>
                                    {errors.username ? (<label htmlFor="email" className={classnames('form__invalid', {'form__invalid-show': errors.username})}>{errors.username}</label>) : (<label htmlFor="email" className="form__label">What you would like to be called?</label>)}
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <div className="form__group">
                                    <input type="text" className={classnames('form__input', {'form__input--is-invalid': errors.firstname})} placeholder="First Name" id="firstname" name="firstname" value={this.state.firstname} onChange={this.onChange}/>
                                    {errors.firstname ? (<label htmlFor="email" className={classnames('form__invalid', {'form__invalid-show': errors.firstname})}>{errors.firstname}</label>) : (<label htmlFor="email" className="form__label">What is your name?</label>)}
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <div className="form__group">
                                    <input type="text" className={classnames('form__input', {'form__input--is-invalid': errors.lastname})} placeholder="Last Name" id="lastname" name="lastname" value={this.state.lastname} onChange={this.onChange}/>
                                    {errors.lastname ? (<label htmlFor="email" className={classnames('form__invalid', {'form__invalid-show': errors.lastname})}>{errors.lastname}</label>) : (<label htmlFor="email" className="form__label">and your last name?</label>)}
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <div className="form__group">
                                    <input type="password" className={classnames('form__input', {'form__input--is-invalid': errors.password})} placeholder="Password" id="password" name="password" maxLength="11" value={this.state.password} onChange={this.onChange}/>
                                    {errors.password ? (<label htmlFor="email" className={classnames('form__invalid', {'form__invalid-show': errors.password})}>{errors.password}</label>) : (<label htmlFor="email" className="form__label">Shhh..Don't tell anyone. It's a secret.</label>)}
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col">
                                <div className="form__group">
                                    <input type="password" className={classnames('form__input', {'form__input--is-invalid': errors.password2})} placeholder="Confirm Password" id="password2" name="password2" maxLength="11" value={this.state.password2} onChange={this.onChange}/>
                                    {errors.password2 ? (<label htmlFor="email" className={classnames('form__invalid', {'form__invalid-show': errors.password2})}>{errors.password2}</label>) : (<label htmlFor="email" className="form__label">Confirm your secret with us.</label>)}
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col">
                                <div className="form__group">
                                    <input type="file" name="avatar" onChange={this.onChange}/>
                                </div>
                            </div>
                        </div>

                        <div className="row u-padding-top-lg">
                            <div className="col">
                                <div className="form__group">
                                    <div className="u-text-align-center">
                                        <button className="btn__ btn__submit" type="submit">Sign Up</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

Register.propTypes = {
    registerUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
});

export default connect(mapStateToProps, {registerUser})(withRouter(Register));
