import React, { Component } from 'react';
import {Link, withRouter} from 'react-router-dom';

import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {logoutUser} from "../../actions/authActions";
import ReactAux from '../../components/hoc/ReactAux';

class Navbar extends Component {

    onLogoutLink (e) {
        e.preventDefault();
        const message = "Successfully logged out.";

        this.props.logoutUser(message, this.props.users);
    }


    render () {
        const {user} = this.props.auth;
        const {staff_user} = this.props.auth.user;



        const cond_sidebar = (
            <ReactAux>
                <li className="sidebar-item"> <Link className="sidebar-link waves-effect waves-dark sidebar-link" to="/messages" aria-expanded="false"><i className="mdi mdi-account-network"/><span className="hide-menu">Messages</span></Link></li>
                <li className="sidebar-item"> <Link className="sidebar-link waves-effect waves-dark sidebar-link" to="/users-analytics" aria-expanded="false"><i className="mdi mdi-border-all"/><span className="hide-menu">Subscribers</span></Link></li>
            </ReactAux>
        );

        return (
            <ReactAux>
                <header className="topbar fixed-top" data-navbarbg="skin5">
                    <nav className="navbar top-navbar navbar-expand-md navbar-dark fixed-top">
                        <div className="navbar-header" data-logobg="skin5">
                            <Link to="/dashboard" className="navbar-brand">
                                <b className="logo-icon">
                                    <img src="http://www.analyticsapi.salesrobot.com/media/default/headerlogo.png" alt="homepage" className="dark-logo navbar__img" />
                                    <img src="http://www.analyticsapi.salesrobot.com/media/default/headerlogo.png" alt="homepage" className="light-logo navbar__img" />
                                </b>
                                <span className="logo-text">
                               SalesRobot Analytics
                          </span>
                            </Link>
                            <div className="nav-toggler waves-effect waves-light d-block d-md-none"><i className="ti-menu ti-close"/></div>
                        </div>
                        <div className="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
                            <ul className="navbar-nav float-left mr-auto">
                                <li className="nav-item"/>
                            </ul>
                            <ul className="navbar-nav float-right">
                                <li className="nav-item dropdown">
                                    <Link className="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" to="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src={`http://www.analyticsapi.salesrobot.com${user.avatar}`} alt="user" className="rounded-circle" width="31" /></Link>
                                    <div className="dropdown-menu dropdown-menu-right user-dd animated">
                                        <Link className="dropdown-item" to=""><i className="ti-user m-r-5 m-l-5"/> My Profile</Link>
                                        <Link className="dropdown-item" to="/users"><i className="fas fa-users m-r-5 m-l-5"/> Users</Link>
                                        <div className="dropdown-divider"/>
                                        <Link to="/" className="dropdown-item" onClick={this.onLogoutLink.bind(this)}><i className="fa fa-power-off m-r-5 m-l-5"/> Logout</Link>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </header>

                <aside className="left-sidebar fixed-left" data-sidebarbg="skin6">
                    <div className="scroll-sidebar">
                        <nav className="sidebar-nav">
                            <ul id="sidebarnav">
                                <li>
                                    <div className="user-profile d-flex no-block dropdown m-t-20">
                                        <div className="user-pic"><img src={`http://www.analyticsapi.salesrobot.com${user.avatar}`} alt="users" className="rounded-circle" width="40" /></div>
                                        <div className="user-content hide-menu m-l-10">
                                            <Link to="javascript:void(0)" className="" id="Userdd" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <h5 className="m-b-0 user-name font-medium">{user.firstname} {user.lastname} <i className="fa fa-angle-down"/></h5>
                                                <span className="op-5 user-email">{user.email}</span>
                                            </Link>
                                            <div className="dropdown-menu dropdown-menu-right" aria-labelledby="User">
                                                <Link className="dropdown-item" to="javascript:void(0)"><i className="ti-settings m-r-5 m-l-5"/> Account Setting</Link>
                                                <div className="dropdown-divider"/>
                                                <Link to="/" className="dropdown-item" onClick={this.onLogoutLink.bind(this)}><i className="fa fa-power-off m-r-5 m-l-5"/> Logout</Link>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li className="sidebar-item"> <Link className="sidebar-link waves-effect waves-dark sidebar-link" to="/dashboard" aria-expanded="false"><i className="mdi mdi-view-dashboard"/><span className="hide-menu">Dashboard</span></Link></li>
                                {staff_user < 3 ? cond_sidebar : null}
                                <li className="sidebar-item"> <Link className="sidebar-link waves-effect waves-dark sidebar-link" to="/archintel" aria-expanded="false"><i class="fas fa-search"/><span className="hide-menu">ArchIntel</span></Link></li>
                            </ul>

                        </nav>
                    </div>
                </aside>
            </ReactAux>
        );
    }
}

Navbar.propTypes = {
    logoutUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    users: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => ({
    auth: state.auth,
    users: state.users
});

export default connect(mapStateToProps, {logoutUser})(withRouter(Navbar));
