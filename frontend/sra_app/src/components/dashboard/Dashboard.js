import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {unloadMessage} from '../../actions/authActions';

class Dashboard extends Component {

    render() {
        return (
            <div className="page-wrapper">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-12">
                            <h4 className="card-subtitle"><i className="fas fa-cog fa-spin"/> Coming Soon</h4>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

Dashboard.propTypes = {
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
});

export default connect(mapStateToProps, {unloadMessage})(Dashboard);
