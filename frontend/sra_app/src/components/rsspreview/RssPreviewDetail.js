import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {getAccountTemplate} from '../../actions/templateActions'
import isEmpty from '../../validations/isEmpty'
import renderHTML from 'react-render-html';
import XMLParser from 'react-xml-parser'

class RssPreviewDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            item: [],
            list_template: '<li style="padding-bottom:5px;"><a href="[URL]" data-type="text" style="text-decoration:none;color:#4599c5;line-height:1;font-size:12px">[TITLE]</a></li>',
            article_template: '<!--[if mso | IE]> <table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" > <tr> <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]--> <div style="background:#ffffff;background-color:#ffffff;Margin:0px auto;max-width:600px;"> <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;" > <tbody> <tr> <td style="border-left:1px solid #CCCCCC;border-right:1px solid #CCCCCC;direction:ltr;font-size:0px;padding:0;text-align:center;vertical-align:top;" ><!--[if mso | IE]> <table role="presentation" border="0" cellpadding="0" cellspacing="0"> <tr> <td class="" style="vertical-align:top;width:600px;" ><![endif]--> <div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;" > <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%" > <tbody> <tr> <td style="vertical-align:top;padding:25px 25px 0 25px;"> <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%" > <table cellspacing="0" cellpadding="0" align="left" class="table"> <tbody> <tr> <td style="padding: 0 !important;"> <table align="left" cellpadding="0" cellspacing="0" class="table"> <tbody> <tr> <td width="5" rowspan="2" style="padding: 0 !important;"> <img style="display: block; margin: 0px; border: 0px; border-radius:7px;" alt="space" src="https://salesrobot.com/uploadimages/image/default/space.png"> </td><td class="left-img-content" style="padding: 0 5px 0 15px; margin: 0;" id="left-img-content-104702987269"> <img style="width: 150px; height: 150px; border-radius: 7px;" src="[IMAGE]" alt="Image" width="100" height="100"> </td><td width="1" rowspan="2" style="padding: 0 !important;"> <img style="display: block; margin: 0px; border: 0px;" alt="space" src="https://salesrobot.com/uploadimages/image/default/space.png"> </td></tr><tr style="padding: 0 !important;"> <td style="text-align: center; padding: 5px 5px 0 15px;"> <span class="left-text-caption" id="left-text-caption-104702987269"></span> </td></tr></tbody> </table> <div class="left-text-content" style="margin: 0 !important; font-family: Ubuntu, Helvetica, Arial, sans-serif; font-size: 13px; line-height: 14px; text-align: justify; color: #000000;" id="left-text-content-104702987269"> <span style="font-size: 10pt;"> <a href="[URL]" style="text-decoration:none;font-size:14px;font-weight: bold; letter-spacing: 1px;"> <strong>[TITLE]</strong> </a> </span> <p style="color:#666666;font-size:14px;line-height:18px"> [CONTENT]<a href="[URL]"> Read More</a> </p></div></td></tr></tbody> </table> </table> </td></tr></tbody> </table> </div><!--[if mso | IE]> </td></tr></table><![endif]--> </td></tr></tbody> </table> </div><!--[if mso | IE]> </td></tr></table> <table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" > <tr> <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]--> <div style="background:#ffffff;background-color:#ffffff;Margin:0px auto;max-width:600px;"> <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;" > <tbody> <tr> <td style="border-left:1px solid #CCCCCC;border-right:1px solid #CCCCCC;direction:ltr;font-size:0px;padding:0px;text-align:center;vertical-align:top;" ><!--[if mso | IE]> <table role="presentation" border="0" cellpadding="0" cellspacing="0"> <tr> <td class="" style="vertical-align:top;width:600px;" ><![endif]--> <div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;" > <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="border-bottom: 1px solid gray" > <tbody> <tr> <td style="vertical-align:top;padding:0px;"> <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%" > <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"> <tbody> <tr style="line-height:15px;font-family:Helvetica, Arial, sans-serif;"> <td align="justify" style="padding-left:20px;padding-bottom:20px;text-align:center;"> <table align="left" cellpadding="0" cellspacing="0" margin="0" padding="0" style="width:auto;border:collapse;display:inline-block;font-family:Helvetica, Arial, sans-serif;"> <tbody> <tr> <td style="padding:2px;"> <span style="font-weight: bold;font-size:14px;text-decoration: none; color: #000000;line-height:15px;display:inline-block;padding:2px 0;font-family:Helvetica, Arial, sans-serif;"> Tags: </span> </td></tr></tbody> </table> [TAG] </td></tr></tbody> </table> </table> </td></tr></tbody> </table> </div><!--[if mso | IE]> </td></tr></table><![endif]--> </td></tr></tbody> </table> </div><!--[if mso | IE]> </td></tr></table><![endif]-->'
        }
        this.createMarkup = this.createMarkup.bind(this)
        this.getRssFeed = this.getRssFeed.bind(this)
    }

    componentDidMount() {
        this.props.getAccountTemplate()
    }

    getRssFeed() {
        fetch('https://cors-anywhere.herokuapp.com/https://executivebiz.secure.force.com/archintelmedia/RSSPrototypeArchintelFeed?id=SOS%20International%20LLC')
            .then(response => response.text())
            .then(data => {
                const xml = new XMLParser().parseFromString(data);
                this.setState({
                    item: xml.getElementsByTagName('item')
                })
            });
    }

    createMarkup() {
        const {list_template, article_template, item} = this.state
        const template = this.props.account_template.map(template => template.template.replace(/\\/g, ''))
        let main_template = ''
        let r_list_temp = ''
        let category = []

        if(isEmpty(item))
            this.getRssFeed()

        // CATEGORY
        item.map(item_parent => {
            item_parent.children.map(item_children => {
                if(item_children.name === 'category') {
                    if(!category.includes(item_children.value)) {
                        category.push(item_children.value)
                    }
                }
            })
        })

        // LIST
        item.map(item_parent => {
            item_parent.children.map(item_children => {
                if(item_children.name === 'link') {
                    r_list_temp += list_template.replace(/\[URL\]/, item_children.value)
                }
            })
        })
        item.map(item_parent => {
            item_parent.children.map(item_children => {
                if(item_children.name === 'title') {
                    r_list_temp = r_list_temp.replace(/\[TITLE\]/, item_children.value)
                }
            })
        })

        console.log('---')
        category.map(category => {
            item.map(item_parent => {
                item_parent.children.map(item_children => {
                    if(item_children.name === 'link') {
                        console.log(item_children.value)
                    }
                })
            })
        })

        try {
            main_template = template[0]
            main_template = main_template.replace(/\[sosiintel-\]/, r_list_temp)

            return {__html: main_template};
        } catch(e) {
            console.log()
        }
    }

    render() {
        const user_content = (
            <div class="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body u-bg-main" style={{textAlign: 'center'}}>
                                    <div style={{display: 'inline-block'}}>
                                        {renderHTML('<style type="text/css">tr,td {border: 0 !important;} ul {padding-left: 40px;}</style>')}
                                        <div dangerouslySetInnerHTML={this.createMarkup()} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
        return (
            <div>
                {user_content}
            </div>
        )
    }
}

RssPreviewDetail.propTypes = {
    auth: PropTypes.object.isRequired,
    users: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired,
    account_template: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors,
    account_template: state.account_template
})

export default connect(mapStateToProps, {getAccountTemplate})(RssPreviewDetail);
