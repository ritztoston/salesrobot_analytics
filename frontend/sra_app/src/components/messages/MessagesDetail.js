import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getMessageDetails, getMessageNextDetails, getMessageDetailsAll, setLoadingCSVTrue, setLoadingCSVFalse} from '../../actions/messagesActions';
import Pace from 'react-pace-progress';
import isEmpty from '../../validations/isEmpty';
import {Link, withRouter} from 'react-router-dom';
import Pagination from "react-js-pagination";
import Moment from 'react-moment';
import 'moment-timezone';
import "react-datepicker/dist/react-datepicker.css";
import classnames from 'classnames';
import { CSVLink } from 'react-csv';

class UserAnalytics extends Component {
    constructor(props) {
        super(props);
        this.state = {
            timezone: 'America/New_York',
            lastpage: false,
            pagination_page: 15,
            current_page: 1,
            filter: 'true'
        };

        this.selectedTimezone = this.selectedTimezone.bind(this);
        this.selectedType = this.selectedType.bind(this);
        this.handlePageChange = this.handlePageChange.bind(this);
        this.downloadCsv = this.downloadCsv.bind(this);
    }

    componentDidMount() {
        const {filter} = this.state;
        if(this.props.match.params.id) {
            this.props.getMessageDetails(this.props.match.params.id, this.props.match.params.account, filter);
            this.props.getMessageDetailsAll(this.props.match.params.id, this.props.match.params.account, 'all');
        }
    }

    handlePageChange(pageNumber) {
        const {filter} = this.state;

        this.props.getMessageNextDetails(this.props.match.params.id, pageNumber, this.props.match.params.account, filter);
        this.setState({current_page: pageNumber})
    }

    selectedTimezone(e) {
        this.setState({timezone: e.target.value})
    }

    downloadCsv(e) {
        e.preventDefault();
        const btn = this.refs.csv;

        this.props.setLoadingCSVTrue();
        setTimeout(() => {
            this.props.setLoadingCSVFalse()
        }, 1500);
        btn.link.click();
    }

    selectedType(e) {
        this.setState({filter: e.target.value});
        this.props.getMessageDetails(this.props.match.params.id, this.props.match.params.account, e.target.value)
    }

    render() {
        const {results, count} = this.props.messageDetails.users;
        const {subject} = this.props.messageDetails;
        const {loading} = this.props.auth;
        const loadingCSV = this.props.messageDetailsCSV.loading;
        const {users} = this.props.messageDetailsCSV.data;
        const {timezone, pagination_page, current_page, filter} = this.state;

        const headers = [
            { label: 'Email', key: 'email' },
            { label: 'URL', key: 'url' },
            { label: 'Clicked', key: 'click' }
        ];
        let csvData = [];
        users.map(user => {
            user.linktrackuml.map(details => {
                details.url = details.url.replace(/"/, "");
                csvData = [...csvData, {email: user.email, url: details.url, click: details.clicked}]
            })
        });

        const usersAnalytics_content = results.map(result => {
            let links;
            let key = 0;
            if(!isEmpty(result.linktrackuml)){
                links = result.linktrackuml.map(linkset => {
                    const date = <Moment format="ddd MMM DD, YYYY h:mm A" add={{ hours: 1 }} tz={timezone}>{linkset.firstclick}</Moment>;
                    key++;
                    return (
                        <tr className="u-bold-text" key={key}>
                            <td>{linkset.url}</td>
                            <td> {linkset.clicked}</td>
                            <td>{date}</td>
                        </tr>
                    )
                })
            }

            const table = (
                <div className="table-responsive">
                    <table className="table v-middle">
                        <thead>
                        <tr className="bg-light">
                            <th className="border-top-0 u-custom-width">Links Clicked</th>
                            <th className="border-top-0">Clicked</th>
                            <th className="border-top-0">Clicked On</th>
                        </tr>
                        </thead>
                        <tbody>
                        {links}
                        </tbody>
                    </table>
                </div>
            );

            return (
                <tr className="custom-tr u-bold-text" key={result.userid}>
                    <td><div className="d-flex align-items-center"><h5>{result.userid}</h5></div></td>
                    <td>{result.email}</td>
                    <td>{!isEmpty(result.linktrackuml) ? table : (<span>- No links clicked -</span>)}</td>
                </tr>
            )
        });

        const umain_content = (
            <div className="page-wrapper">
                <div className="page-breadcrumb">
                    <div className="row align-items-center">
                        <div className="col-5">
                            <h4 className="page-title">Messages</h4>
                            <div className="d-flex align-items-center">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item"><Link to="/messages">Messages</Link></li>
                                        <li className="breadcrumb-item active u-bold-text" aria-current="page">Message Details</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                        <div className="col-7">
                            <div className="text-right upgrade-btn">
                                <h5 className="page-title">
                                    <button className="btn__ btn__success" title="Export CSV file" disabled={loadingCSV} onClick={this.downloadCsv}>{!loadingCSV ? (<span><i className="fas fa-file-csv"/> Export CSV</span>) : null} <div className={classnames('ld ld-spin', {'ld-ring': loadingCSV})}/></button>
                                    <CSVLink ref="csv" filename={subject+'.csv'} data={csvData} style={{display:'none'}} headers={headers} target="_blank"/>
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-12">
                            <div className="card">
                                <div className="card-body">
                                    <div className="d-md-flex align-items-center">
                                        <div>
                                            <h4 className="card-title">Subsriber Details</h4>
                                            <h5 className="card-subtitle">Overview of Subscribers</h5>
                                        </div>
                                        <div className="ml-auto">
                                            <div className="dl">
                                                <div className="container-fluid">
                                                    <div className="row">
                                                        <div className="col-auto">
                                                            <h5 className="u-padding-top-6">Type:</h5>
                                                        </div>
                                                        <div className="auto">
                                                            <select className="custom-select" value={filter} onChange={this.selectedType}>
                                                                <option value="true">Clicked Only</option>
                                                                <option value="false">All Subscribers</option>
                                                                ""                                              </select>
                                                        </div>
                                                        <div className="col-auto">
                                                            <h5 className="u-padding-top-6">Timezone:</h5>
                                                        </div>
                                                        <div className="col-auto">
                                                            <select className="custom-select" value={timezone} onChange={this.selectedTimezone}>
                                                                <option value="America/New_York">EST</option>
                                                                <option value="Asia/Manila">PST</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="table-responsive">
                                    <table className="table v-middle">
                                        <thead>
                                        <tr className="bg-light">
                                            <th className="border-top-0">ID</th>
                                            <th className="border-top-0">Email</th>
                                            <th className="border-top-0">Link Details</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {usersAnalytics_content}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12 u-text-align-center">
                            <div className="pagination-container">
                                <Pagination
                                    activePage={current_page}
                                    itemsCountPerPage={pagination_page}
                                    totalItemsCount={count}
                                    pageRangeDisplayed={5}
                                    onChange={this.handlePageChange}
                                    activeLinkClass="active-link"
                                    hideDisabled={true}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );

        return (
            <div>
                {loading ? <Pace color="#FA7921" height={4}/> : umain_content}
            </div>
        )
    }
}

UserAnalytics.propTypes = {
    getMessageDetails: PropTypes.func.isRequired,
    getMessageDetailsAll: PropTypes.func.isRequired,
    setLoadingCSVTrue: PropTypes.func.isRequired,
    setLoadingCSVFalse: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    users: PropTypes.array.isRequired,
    messageDetails: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth,
    users: state.users,
    messageDetails: state.messageDetails,
    messageDetailsCSV: state.messageDetailsCSV,
    errors: state.errors
});


export default connect(mapStateToProps, {getMessageDetails, getMessageNextDetails, getMessageDetailsAll, setLoadingCSVTrue, setLoadingCSVFalse})(withRouter(UserAnalytics));
