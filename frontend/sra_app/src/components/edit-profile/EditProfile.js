import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import CommonTextFieldGroup from '../common/CommonTextFieldGroup';
import isEmpty from '../../validations/isEmpty';
import {editUser} from '../../actions/authActions';
import Pace from 'react-pace-progress';
import {Link, withRouter} from 'react-router-dom';

import classnames from 'classnames';

class EditProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            username: '',
            firstname: '',
            lastname: '',
            avatar: '',
            previewURL: '',
            isSet: false,
            changed: false,

            o_email: '',
            o_username: '',
            o_firstname: '',
            o_lastname: '',
        };
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    static getDerivedStateFromProps(nextProps, prevState) {

        if(nextProps.auth.user && !prevState.isSet) {
            const user = nextProps.auth.user;

            user.email = !isEmpty(user.email) ? user.email : '';
            user.username = !isEmpty(user.username) ? user.username : '';
            user.firstname = !isEmpty(user.firstname) ? user.firstname : '';
            user.lastname = !isEmpty(user.lastname) ? user.lastname : '';
            user.avatar = !isEmpty(user.avatar) ? user.avatar : '';

            return ({
                email: user.email,
                o_email: user.email,
                username: user.username,
                o_username: user.username,
                firstname: user.firstname,
                o_firstname: user.firstname,
                lastname: user.lastname,
                o_lastname: user.lastname,
                previewURL: user.avatar,
                o_avatar: user.avatar,
                isSet: true
            })
        }

        if(prevState.isSet) {
            if (prevState.o_avatar !== prevState.previewURL) {
                return ({changed: true})
            }
            if(prevState.o_username !== prevState.username) {
                return ({changed: true})
            }
            else if(prevState.o_email !== prevState.email) {
                return ({changed: true})
            }
            else if(prevState.o_firstname !== prevState.firstname) {
                return ({changed: true})
            }
            else if(prevState.o_lastname !== prevState.lastname) {
                return ({changed: true})
            }
            else {
                return ({changed: false})
            }
        }

        return null
    }

    onChange(e) {
        if(e.target.name !== 'avatar'){
            this.setState({[e.target.name]: e.target.value});
        }
        else {
            const stateName = e.target.name;
            const reader = new FileReader();
            const file = e.target.files[0];

            reader.onloadend = () => {
                this.setState({
                    [stateName]: file,
                    previewURL: reader.result
                })
            };

            reader.readAsDataURL(file)
        }
    }

    onSubmit(e) {
        e.preventDefault();
        const message = 'Successfully edited profile.';

        const newUser = {
            email: this.state.email,
            username: this.state.username,
            firstname: this.state.firstname,
            lastname: this.state.lastname,
            avatar: this.state.avatar
        };

        this.props.editUser(newUser, message, this.props.errors, this.props.auth.user.user_id, this.props.history)
    }

    render() {
        const {errors} = this.props;
        const {loading} = this.props.auth;
        const {changed} = this.state;

        let disabled = false;
        if(changed) {
            if(loading) {
                disabled = loading
            } else {
                disabled = loading
            }
        } else {
            disabled = !changed
        }

        return (
            <div>
                {loading ? <Pace color="#FA7921" height={4}/> : null}
                <div className="container">
                    <div className="u-margin-top-lg u-heading-primary">
                        Edit Profile
                    </div>

                    <div className="edit__form">
                        <form action="#" className="form" autoComplete="off" onSubmit={this.onSubmit} encType="application/json">
                            <div className="container">
                                <div className="row">
                                    <div className="col u-text-align-center">
                                        <div className="parent_photo">
                                            <img className="rounded-circle profile__photo" src={this.state.previewURL} alt="avatar" title="avatar"/>
                                            <input className="rounded-circle file_upload" type="file" name="avatar" accept="image/*" onChange={this.onChange} title="Change user photo"/>

                                            {errors.avatar ? (<label htmlFor="email" className={classnames('form__invalid', {'form__invalid-show': errors.avatar})}>{errors.avatar}</label>) : null}
                                        </div>
                                    </div>
                                </div>

                                <div className="row u-margin-top-lg">
                                    <div className="col">
                                        <CommonTextFieldGroup
                                            placeholder="Username"
                                            placeholder_info="Username"
                                            type="text"
                                            value={this.state.username}
                                            onChange={this.onChange}
                                            error={errors.username}
                                            name="username"
                                        />
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col">
                                        <CommonTextFieldGroup
                                            placeholder="Email"
                                            placeholder_info="Email"
                                            type="text"
                                            value={this.state.email}
                                            onChange={this.onChange}
                                            error={errors.email}
                                            name="email"
                                        />
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col">
                                        <CommonTextFieldGroup
                                            placeholder="First name"
                                            placeholder_info="First name"
                                            type="text"
                                            value={this.state.firstname}
                                            onChange={this.onChange}
                                            error={errors.firstname}
                                            name="firstname"
                                        />
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col">
                                        <CommonTextFieldGroup
                                            placeholder="Last name"
                                            placeholder_info="Last name"
                                            type="text"
                                            value={this.state.lastname}
                                            onChange={this.onChange}
                                            error={errors.lastname}
                                            name="lastname"
                                        />
                                    </div>
                                </div>

                                <div className="row u-padding-top-lg">
                                    <div className="col">
                                        <div className="form__group">
                                            <div className="u-text-align-center">
                                                <Link to="/dashboard" className="btn__ btn__cancel">Cancel</Link>
                                                <button className="btn__ btn__submit" type="submit" disabled={disabled}>Confirm <div className={classnames('ld ld-spin', {'ld-ring': loading})}/></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>

                </div>
            </div>
        )
    }
}

EditProfile.propTypes = {
    editUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
});

export default connect(mapStateToProps, {editUser})(withRouter(EditProfile))
