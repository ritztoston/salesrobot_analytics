import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getMessageDetails, getMessageNextDetails, getMessageDetailsAll, setLoadingCSVTrue, setLoadingCSVFalse, getRSSMessageDetails} from '../../actions/messagesActions';
import Pace from 'react-pace-progress';
import isEmpty from '../../validations/isEmpty';
import {Link, withRouter} from 'react-router-dom';
import Pagination from "react-js-pagination";
import Moment from 'react-moment';
import 'moment-timezone';
import 'react-datepicker/dist/react-datepicker.css';
import classnames from 'classnames';
import { CSVLink } from "react-csv";
import CountUp from 'react-countup';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import ChartistGraph from 'react-chartist';
import { Base64 } from 'js-base64';

class UserAnalytics extends Component {
    constructor(props) {
        super(props);
        this.state = {
            timezone: 'America/New_York',
            lastpage: false,
            pagination_page: 15,
            current_page: 1,
            filter: 'true',
            type_filter: 'Clicked Only',
            view_filter: 'Graphical',
            tabIndex: 0,
            categories: [
                {name: 'sosinews', new_name: 'SOSi in the News'},
                {name: 'sosiintel', new_name: 'Intelligence Solutions Group'},
                {name: 'sosimission', new_name: 'Mission Solutions Group'},
                {name: 'soscsg', new_name: 'Cyber Solutions Group'},
                {name: 'sosssg', new_name: 'Software Solutions Group'},
                {name: 'sosicoin', new_name: 'Competitor Intelligence'},
                {name: 'sosiconwin', new_name: 'Contract Wins'},
                {name: 'sosilegis', new_name: 'Legislative Affairs'},
                {name: 'geoact', new_name: 'Geopolitical Activity'}
            ]
        };

        this.selectedTimezone = this.selectedTimezone.bind(this);
        this.selectedType = this.selectedType.bind(this);
        this.handlePageChange = this.handlePageChange.bind(this);
        this.downloadCsv = this.downloadCsv.bind(this);
    }

    componentDidMount() {
        const {filter} = this.state;
        if(this.props.match.params.id) {
            let newid = Base64.decode(this.props.match.params.id);
            newid = newid/1500;
            this.props.getMessageDetails(newid, this.props.match.params.account, filter);
            this.props.getMessageDetailsAll(newid, this.props.match.params.account, 'all');
            this.props.getRSSMessageDetails(this.props.match.params.account, newid);
        }
    }

    handlePageChange(pageNumber) {
        const {filter} = this.state;
        let newid = Base64.decode(this.props.match.params.id);
        newid = newid/1500;

        this.props.getMessageNextDetails(newid, pageNumber, this.props.match.params.account, filter);
        this.setState({current_page: pageNumber})
    }

    selectedTimezone(e) {
        this.setState({timezone: e.target.value})
    }

    downloadCsv(e) {
        e.preventDefault();
        const btn = this.refs.csv;

        this.props.setLoadingCSVTrue();
        setTimeout(() => {
            this.props.setLoadingCSVFalse()
        }, 1500);
        btn.link.click();
    }

    selectedType(e) {
        let newid = Base64.decode(this.props.match.params.id);
        newid = newid/1500;
        this.setState({type_filter: e.target.value});
        this.props.getMessageDetails(newid, this.props.match.params.account, e.target.value)
    }

    render() {
        const {results, count} = this.props.messageDetails.users;
        const rss_results = this.props.rss_message_details;
        const account = this.props.match.params.account;
        const {id, subject, sent, viewed, processed, fromfield} = this.props.messageDetails;
        const unique_clicks = this.props.messageDetails.users.count;
        const {loading} = this.props.auth;
        const loadingCSV = this.props.messageDetailsCSV.loading;
        const {users} = this.props.messageDetailsCSV.data;
        const {timezone, pagination_page, current_page, tabIndex, categories, type_filter,} = this.state;
        const replaced_date = (<span><Moment format="MM/DD/YYYY" tz={timezone}>{sent}</Moment></span>);
        let ok_replace_date;
        let new_subject;
        let piedata;
        let options;
        let percent_viewed;
        let percent_processed;
        let responsiveOptions;
        let date_sent;
        let sender;
        let title;
        if(!isEmpty(viewed) && !isEmpty(processed)) {
            percent_viewed = Math.round(viewed/processed * 100);
            percent_processed = Math.round((processed-viewed)/processed * 100);
            piedata = {
                labels: [percent_viewed + '%', percent_processed + '%'],
                series: [(processed-viewed), viewed]
            };

            options = {
                fullWidth: true,
                height: 330,
                donut: true,
                showLabel: false
            };

            responsiveOptions = [
                ['(min-width: 1200px) and (max-width: 1380px)', {
                    height: 280
                }]
            ];
        }

        if(!isEmpty(subject)) {
            ok_replace_date = subject.includes("[TODAY:m/d/Y]");
            if(ok_replace_date) new_subject = subject.replace("[TODAY:m/d/Y]", " ");
            else new_subject = subject;

            date_sent = <Moment format="ddd MMM DD, YYYY h:mm A" add={{ hours: 1 }} tz={timezone}>{sent}</Moment>;

            sender = fromfield.substr(0,fromfield.indexOf(' '));
            title = fromfield.substr(fromfield.indexOf(' ')+1);
        }

        const headers = [
            { label: 'Email', key: 'email' },
            { label: 'URL', key: 'url' },
            { label: 'Clicked', key: 'click' }
        ];
        let csvData = [];
        users.map(user => {
            user.linktrackuml.map(details => {
                details.url = details.url.replace(/"/, "");
                csvData = [...csvData, {email: user.email, url: details.url, click: details.clicked}]
            })
        });

        const usersAnalytics_content = results.map(result => {
            let links;
            let key = 0;
            if(!isEmpty(result.linktrackuml)){
                links = result.linktrackuml.map(linkset => {
                    const date = <Moment format="ddd MMM DD, YYYY h:mm A" add={{ hours: 1 }} tz={timezone}>{linkset.firstclick}</Moment>;
                    key++;
                    return (
                        <tr className="u-bold-text" key={key}>
                            <td>{linkset.url}</td>
                            <td> {linkset.clicked}</td>
                            <td>{date}</td>
                        </tr>
                    )
                })
            }

            const table = (
                <div className="table-responsive">
                    <table className="table v-middle">
                        <thead>
                        <tr className="bg-light">
                            <th className="border-top-0 u-custom-width">Links Clicked</th>
                            <th className="border-top-0">Clicked</th>
                            <th className="border-top-0">Clicked On</th>
                        </tr>
                        </thead>
                        <tbody>
                        {links}
                        </tbody>
                    </table>
                </div>
            );

            return (
                <tr className="custom-tr u-bold-text" key={result.userid}>
                    <td><div className="d-flex align-items-center"><h5>{result.userid}</h5></div></td>
                    <td>{result.email}</td>
                    <td>{!isEmpty(result.linktrackuml) ? table : (<span>- No links clicked -</span>)}</td>
                </tr>
            )
        });

        const rssMessage_content = rss_results.map(result => {
            const new_cat = categories.map(category => {
                if (category.name === result.category)
                    return category.new_name
            });

            return (
                <tr className="custom-tr u-bold-text" key={result.itemid}>
                    <td><img src={result.image} alt="homepage" className="dark-logo navbar__img" /></td>
                    <td>{result.title}</td>
                    <td><a className="u-bluelight-text" href={`${result.url}`} target="_blank"><i className="fas fa-external-link-alt"/></a></td>
                    <td>{new_cat}</td>
                    <td>{result.clicked === 0 ? (<span className="u-error-text">0</span>) : (<span className="u-success-text"><h5>{result.clicked}</h5></span>)}</td>
                </tr>
            )
        });

        const tabular_content = (
            <div className="table-responsive">
                <table className="table v-middle">
                    <thead>
                    <tr className="bg-light">
                        <th className="border-top-0">Image</th>
                        <th className="border-top-0">Title</th>
                        <th className="border-top-0">Link</th>
                        <th className="border-top-0">Category</th>
                        <th className="border-top-0">Clicked</th>
                    </tr>
                    </thead>
                    <tbody>
                    {rssMessage_content}
                    </tbody>
                </table>
            </div>
        );

        const umain_content = (
            <div className="page-wrapper">
                <div className="page-breadcrumb">
                    <div className="row align-items-center">
                        <div className="col-5">
                            <h4 className="page-title">Messages</h4>
                            <div className="d-flex align-items-center">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item"><Link to="/archintel">Accounts</Link></li>
                                        <li className="breadcrumb-item"><Link to={`/archintel/${account}`}>Messages</Link></li>
                                        <li className="breadcrumb-item active u-bold-text" aria-current="page">Message Details</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-12">
                            <Tabs selectedIndex={tabIndex} onSelect={tabIndex => this.setState({ tabIndex })}>
                                <TabList style={{border: '0', margin: '0'}}>
                                    <Tab style={{border: '0'}}><span  className={classnames('custom__tabs', {'custom__tabs-uncheck': tabIndex !== 0})}>Overview</span></Tab>
                                    <Tab style={{border: '0'}}><span className={classnames('custom__tabs', {'custom__tabs-uncheck': tabIndex !== 1})}>Campaign Stories</span></Tab>
                                    <Tab style={{border: '0'}}><span className={classnames('custom__tabs', {'custom__tabs-uncheck': tabIndex !== 2})}>Subscribers Clicked</span></Tab>
                                </TabList>

                                <TabPanel>
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="d-md-flex align-items-center">
                                                <div className="u-margin-top-m">
                                                    <h4 className="card-title">Campaign: {new_subject}{ok_replace_date ? replaced_date : null}</h4>
                                                    <h5 className="card-subtitle">Overview of Campaign ID#{id}</h5>
                                                </div>
                                            </div>
                                            <div className="row u-margin-top-sm">
                                                <div className="col-12">
                                                    <div className="row">
                                                        <div className="col-3 u-padding-left-xl">
                                                            <div className="row u-margin-top-sm">

                                                                <div className="col-12">
                                                                    <span className="u-heading-primary-heavy u-block-text u-orange-text"><CountUp end={processed} /></span>
                                                                    <h5><i className="fas fa-user"/> Delivered</h5>
                                                                </div>
                                                            </div>
                                                            <div className="row u-margin-top-sm">
                                                                <div className="col-6 u-vertical-align">
                                                                    <span className="u-bluelight-text u-heading-primary u-block-text"><CountUp end={viewed} /></span>
                                                                    <h5><i className="fas fa-eye"/> Opened</h5>
                                                                </div>
                                                                <div className="col-6">
                                                                    <span className="u-heading-primary u-block-text u-yellow-text"><CountUp end={unique_clicks} /></span>
                                                                    <h5><i className="fas fa-mouse-pointer"/> Clicked</h5>
                                                                </div>
                                                            </div>
                                                            <div className="row u-margin-top-sm">
                                                                <div className="col-6">
                                                                    <span className="u-heading-primary u-block-text u-error-text"><CountUp end={0} /></span>
                                                                    <h5><i className="fas fa-exclamation-circle"/> Bounced</h5>
                                                                </div>
                                                                <div className="col-6">
                                                                    <span className="u-heading-primary u-block-text u-error-text"><CountUp end={0} />%</span>
                                                                    <h5>Bounced %</h5>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-4 u-text-align-center">
                                                            <div className="row u-margin-top-m">
                                                                <div className="col-6">
                                                                    <h3 className="u-violet-text"><CountUp end={percent_viewed} />%</h3>
                                                                    <h5>Opened %</h5>
                                                                </div>
                                                                <div className="col-6">
                                                                    <h3 className="u-bluelight-text"><CountUp end={percent_processed} />%</h3>
                                                                    <h5>Unopened %</h5>
                                                                </div>
                                                            </div>
                                                            <ChartistGraph key="1" className={'ct-chart'} data={piedata}  type="Pie" options={options} responsiveOptions={responsiveOptions}/>
                                                        </div>
                                                        <div className="col-5 u-padding-left-xl">
                                                            <div className="row u-margin-top-m">
                                                                <div className="col-12">
                                                                    <span className="u-heading-m u-block-text">{date_sent}</span>
                                                                    <h5><i className="fas fa-clock"/> Sent</h5>
                                                                </div>
                                                            </div>
                                                            <div className="row u-margin-top-m">
                                                                <div className="col-12">
                                                                    <span className="u-heading-s u-block-text">{new_subject}{ok_replace_date ? replaced_date : null}</span>
                                                                    <h5><i className="far fa-newspaper"/> Subject</h5>
                                                                </div>
                                                            </div>
                                                            <div className="row u-margin-top-m">
                                                                <div className="col-12">
                                                                    <span className="u-heading-s u-block-text">{title}</span>
                                                                    <h5><i className="fas fa-newspaper"/> Title</h5>
                                                                </div>
                                                            </div>
                                                            <div className="row u-margin-top-m">
                                                                <div className="col-12">
                                                                    <span className="u-heading-s u-block-text">{sender}</span>
                                                                    <h5><i className="fas fa-envelope"/> From</h5>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </TabPanel>
                                <TabPanel>
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="d-md-flex align-items-center">
                                                <div className="u-margin-top-m">
                                                    <h4 className="card-title">Campaign Details</h4>
                                                    <h5 className="card-subtitle">Overview of Campaign Stories</h5>
                                                </div>
                                            </div>
                                        </div>
                                        {tabular_content}
                                    </div>
                                </TabPanel>
                                <TabPanel>
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="d-md-flex align-items-center">
                                                <div className="u-margin-top-m">
                                                    <h4 className="card-title">Subsriber Details</h4>
                                                    <h5 className="card-subtitle">Overview of Subscribers</h5>
                                                </div>
                                                <div className="ml-auto">
                                                    <div className="dl">
                                                        <div className="container-fluid">
                                                            <div className="row">
                                                                <div className="col-auto">
                                                                    <h5 className="u-padding-top-6">Type:</h5>
                                                                </div>
                                                                <div className="auto">
                                                                    <select className="custom-select" value={type_filter} onChange={this.selectedType}>
                                                                        <option value="Clicked Only">Clicked Only</option>
                                                                        <option value="All Subscribers">All Subscribers</option>
                                                                    </select>
                                                                </div>
                                                                <div className="col-auto">
                                                                    <h5 className="u-padding-top-6">Timezone:</h5>
                                                                </div>
                                                                <div className="col-auto">
                                                                    <select className="custom-select" value={timezone} onChange={this.selectedTimezone}>
                                                                        <option value="America/New_York">EST</option>
                                                                        <option value="Asia/Manila">PST</option>
                                                                    </select>
                                                                </div>
                                                                <div className="col-auto">
                                                                    <div className="text-right upgrade-btn">
                                                                        <h5 className="page-title">
                                                                            <button className="btn__ btn__success" title="Export CSV file" disabled={loadingCSV} onClick={this.downloadCsv}>{!loadingCSV ? (<span><i className="fas fa-file-csv"/> Export CSV</span>) : null} <div className={classnames('ld ld-spin', {'ld-ring': loadingCSV})}/></button>
                                                                            <CSVLink ref="csv" filename={subject+'.csv'} data={csvData} style={{display:'none'}} headers={headers} target="_blank"/>
                                                                        </h5>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="table-responsive">
                                            <table className="table v-middle">
                                                <thead>
                                                <tr className="bg-light">
                                                    <th className="border-top-0">ID</th>
                                                    <th className="border-top-0">Email</th>
                                                    <th className="border-top-0">Link Details</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {usersAnalytics_content}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-12 u-text-align-center">
                                            <div className="pagination-container">
                                                <Pagination
                                                    activePage={current_page}
                                                    itemsCountPerPage={pagination_page}
                                                    totalItemsCount={count}
                                                    pageRangeDisplayed={5}
                                                    onChange={this.handlePageChange}
                                                    activeLinkClass="active-link"
                                                    hideDisabled={true}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </TabPanel>
                            </Tabs>
                        </div>
                    </div>
                </div>
            </div>
        );

        return (
            <div>
                {loading ? <Pace color="#FA7921" height={4}/> : umain_content}
            </div>
        )
    }
}

UserAnalytics.propTypes = {
    getMessageDetails: PropTypes.func.isRequired,
    getMessageDetailsAll: PropTypes.func.isRequired,
    getRSSMessageDetails: PropTypes.func.isRequired,
    setLoadingCSVTrue: PropTypes.func.isRequired,
    setLoadingCSVFalse: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    users: PropTypes.array.isRequired,
    messageDetails: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth,
    users: state.users,
    messageDetails: state.messageDetails,
    messageDetailsCSV: state.messageDetailsCSV,
    rss_message_details: state.rss_message_details,
    errors: state.errors
});


export default connect(mapStateToProps, {getMessageDetails, getMessageNextDetails, getMessageDetailsAll, setLoadingCSVTrue, setLoadingCSVFalse, getRSSMessageDetails})(withRouter(UserAnalytics));
