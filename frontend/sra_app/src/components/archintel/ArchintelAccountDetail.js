import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getAllMessages, getMessageNextList} from '../../actions/messagesActions';
import Pace from 'react-pace-progress';
import {Link, withRouter} from 'react-router-dom';
import Pagination from "react-js-pagination";
import Moment from 'react-moment';
import 'moment-timezone';
import "react-datepicker/dist/react-datepicker.css";
import ChartistGraph from "react-chartist";
import Chartist from 'chartist';
import { Base64 } from 'js-base64';

class ArchintelAccountDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            timezone: 'America/New_York',
            pagination_page: 15,
            current_page: 1
        };
        this.linkClick = this.linkClick.bind(this);
        this.handleChangeDate = this.handleChangeDate.bind(this);

        this.selectedAccount = this.selectedAccount.bind(this);
        this.selectedTimezone = this.selectedTimezone.bind(this);
        this.handlePageChange = this.handlePageChange.bind(this);
    }

    componentDidMount() {
        const account = this.props.match.params.account;
        // if(results === undefined || results.length < 1)
        this.props.getAllMessages(account)
    }

    handleChangeDate(date) {
        this.setState({
            startDate: date
        });
    }

    linkClick(id) {
        const account = this.props.match.params.account;
        const newid = Base64.encode(id*1500);
        this.props.history.push(`/archintel/${account}/messages/${newid}`)
    }

    handlePageChange(pageNumber) {
        const account = this.props.match.params.account;

        this.props.getMessageNextList(pageNumber, account);
        this.setState({current_page: pageNumber})
    }

    selectedTimezone(e) {
        this.setState({timezone: e.target.value})
    }

    selectedAccount(e) {
        this.setState({account: e.target.value});
        this.props.getAllMessages(e.target.value)
    }

    render() {
        const {results, count} = this.props.messages;
        const {loading} = this.props.auth;
        const {timezone, pagination_page, current_page} = this.state;
        let labels1 = [];
        let dataSeries1 = [];
        let dataSeries2 = [];
        let dataBar1 = [];
        let dataBar2 = [];

        let data1 = {
            labels: labels1,
            series: [
                {
                    name: 'series-1',
                    data: dataSeries1
                },
                {
                    name: 'series-2',
                    data: dataSeries2
                }
            ]
        };
        let data2 = {
            labels: labels1,
            series: [
                {
                    name: 'series-1',
                    data: dataBar2
                },
                {
                    name: 'series-2',
                    data: dataBar1
                }
            ]
        };

        const options1 = {
            plugins: [Chartist.plugins.tooltip()],
            low: 0,
            fullWidth: true,
            height: 243,
            showArea: true,
            series: {
                'series-1': {
                    lineSmooth: Chartist.Interpolation.simple(),
                    showArea: true
                },
                'series-2': {
                    lineSmooth: Chartist.Interpolation.simple(),
                    showPoint: true
                }
            }
        };
        const options2 = {
            low: 0,
            stackBars: true,
            fullWidth: true,
            height: 243
        };

        const responsiveOptions = [
            ['(min-width: 1200px) and (max-width: 1380px)', {
                height: 203
            }]
        ];

        const type1 = 'Line';
        const type2 = 'Bar';

        const graph = (
            <ChartistGraph key="1" className={'ct-chart'} data={data1}  options={options1} type={type1} listener={{"draw" : function(data) {
                    if(data.type === 'line' || data.type === 'area') {
                        data.element.animate({
                            d: {
                                begin: 400 * data.index,
                                dur: 2000,
                                from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
                                to: data.path.clone().stringify(),
                                easing: Chartist.Svg.Easing.easeOutQuint
                            }
                        });
                    } } }}/>
        );

        const bargraph = (
            <ChartistGraph key="1" className={'ct-chart'} data={data2}  options={options2} type={type2} responsiveOptions={responsiveOptions}/>
        );

        const usersAnalytics_content = results.map(result => {
            const date = <Moment format="ddd MMM DD, YYYY h:mm A" add={{ hours: 1 }} tz={timezone}>{result.sent}</Moment>;
            const replaced_date = (<span><Moment format="MM/DD/YYYY" tz={timezone}>{result.sent}</Moment></span>);
            let subject = '';
            let ok_replace_date;
            labels1.push(result.id);
            dataSeries1.push({meta: 'Viewed', value: result.viewed});
            dataSeries2.push({meta: 'Unique Views', value: result.unique_views});

            dataBar1.push({meta: 'Total URLS', value: result.availurlscount});
            dataBar2.push({meta: 'URLS Clicked', value: result.availurlsclickedcount});

            ok_replace_date = result.subject.includes("[TODAY:m/d/Y]");

            if(ok_replace_date) subject = result.subject.replace("[TODAY:m/d/Y]", " ");
            else subject = result.subject;

            return (
                <tr onClick={() => this.linkClick(result.id)} key={result.id} className="custom-tr u-bold-text">
                    <td><h5>{subject}{ok_replace_date ? replaced_date : null}</h5></td>
                    <td>{result.processed.toLocaleString(navigator.language, { minimumFractionDigits: 0 })}</td>
                    <td>{result.viewed.toLocaleString(navigator.language, { minimumFractionDigits: 0 })}</td>
                    <td>{result.unique_views.toLocaleString(navigator.language, { minimumFractionDigits: 0 })}</td>
                    <td>{result.availurlsclickedcount.toLocaleString(navigator.language, { minimumFractionDigits: 0 })}</td>
                    <td>{date}</td>
                </tr>
            )
        });

        const umain_content = (
            <div className="page-wrapper">
                <div className="page-breadcrumb">
                    <div className="row align-items-center">
                        <div className="col-5">
                            <h4 className="page-title">Messages</h4>
                            <div className="d-flex align-items-center">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item"><Link to="/archintel">Accounts</Link></li>
                                        <li className="breadcrumb-item active u-bold-text" aria-current="page">Messages</li>
                                        <li className="breadcrumb-item active" aria-current="page">Message Details</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-8">
                            <div className="card">
                                <div className="card-body">
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="d-md-flex align-items-center">
                                                <div>
                                                    <h4 className="card-title">Views Summary</h4>
                                                    <h5 className="card-subtitle">Overview of Recent Sent Messages</h5>
                                                </div>
                                                <div className="ml-auto d-flex no-block align-items-center">
                                                    <ul className="list-inline font-12 dl m-r-15 m-b-0">
                                                        <li className="list-inline-item u-bluedark-text"><i className="fa fa-circle"/> Viewed</li>
                                                        <li className="list-inline-item u-violet-text"><i className="fa fa-circle"/> Unique Views</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-lg-12">
                                                    {graph}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="card">
                                <div className="card-body">
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="d-md-flex align-items-center">
                                                <div>
                                                    <h4 className="card-title">Messages Summary</h4>
                                                    <h5 className="card-subtitle">Overview of Recent URLS clicked</h5>
                                                </div>
                                                <div className="ml-auto d-flex no-block align-items-center">
                                                    <ul className="list-inline font-12 dl m-r-15 m-b-0">
                                                        <li className="list-inline-item u-violet-text"><i className="fa fa-circle"/> Total URLS</li>
                                                        <li className="list-inline-item u-bluedark-text"><i className="fa fa-circle"/> URLS Clicked</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-lg-12">
                                                    {bargraph}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12">
                            <div className="card">
                                <div className="card-body">
                                    <div className="d-md-flex align-items-center">
                                        <div>
                                            <h4 className="card-title">Subscribers Details</h4>
                                            <h5 className="card-subtitle">Overview of Subscribers</h5>
                                        </div>
                                        <div className="ml-auto">
                                            <div className="dl">
                                                <div className="container-fluid">
                                                    <div className="row">
                                                        <div className="col-auto">
                                                            <h5 className="u-padding-top-6">Timezone:</h5>
                                                        </div>
                                                        <div className="col-auto">
                                                            <select className="custom-select" value={timezone} onChange={this.selectedTimezone}>
                                                                <option value="America/New_York">EST</option>
                                                                <option value="Asia/Manila">PST</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="table-responsive">
                                    <table className="table v-middle">
                                        <thead>
                                        <tr className="bg-light">
                                            <th className="border-top-0">Subject</th>
                                            <th className="border-top-0">Sent</th>
                                            <th className="border-top-0">Viewed</th>
                                            <th className="border-top-0">Unique Views</th>
                                            <th className="border-top-0">Clicked</th>
                                            <th className="border-top-0">Sent</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {usersAnalytics_content}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12 u-text-align-center">
                            <div className="pagination-container">
                                <Pagination
                                    activePage={current_page}
                                    itemsCountPerPage={pagination_page}
                                    totalItemsCount={count}
                                    pageRangeDisplayed={5}
                                    onChange={this.handlePageChange}
                                    activeLinkClass="active-link"
                                    hideDisabled={true}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );

        return (
            <div>
                {loading ? <Pace color="#FA7921" height={4}/> : umain_content}
            </div>
        )
    }
}

ArchintelAccountDetail.propTypes = {
    getAllMessages: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    users: PropTypes.array.isRequired,
    messages: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
    auth: state.auth,
    users: state.users,
    messages: state.messages,
    errors: state.errors
});


export default connect(mapStateToProps, {getAllMessages, getMessageNextList})(withRouter(ArchintelAccountDetail));
