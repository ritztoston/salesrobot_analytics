import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import stringToBinary from '../../utils/stringToBinary';

class Archintel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            accounts: [
                {
                    'account': 'SOS International LLC',
                    'name': 'sosi'
                },
            ]
        };

        this.linkClick = this.linkClick.bind(this)
    }

    linkClick(account) {
        this.props.history.push(`/archintel/${account}`)
    }

    render() {
        const {accounts} = this.state;


        const profile_content = accounts.map(account => {
            const id = stringToBinary(account.name);
            return (
                <tr  onClick={() => this.linkClick(account.name)} key={id} className="custom-tr">
                    <td>
                        <h4 className="m-b-0 font-16 u-margin-left-s">{account.account}</h4>
                    </td>
                </tr>
            )
        });

        const user_content = (
            <div className="page-wrapper">
                <div className="page-breadcrumb">
                    <div className="row align-items-center">
                        <div className="col-5">
                            <h4 className="page-title">ArchIntel Accounts</h4>
                            <div className="d-flex align-items-center">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item active u-bold-text" aria-current="page">Accounts</li>
                                        <li className="breadcrumb-item active" aria-current="page">Messages</li>
                                        <li className="breadcrumb-item active" aria-current="page">Message Details</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-12">
                            <div className="card">
                                <div className="card-body">
                                    <div className="d-md-flex align-items-center">
                                        <div>
                                            <h4 className="card-title">ArchIntel Account Details</h4>
                                            <h5 className="card-subtitle">Overview of ArchIntel Accounts</h5>
                                        </div>
                                    </div>
                                </div>
                                <div className="table-responsive">
                                    <table className="table v-middle">
                                        <thead>
                                        <tr className="bg-light">
                                            <th className="border-top-0">Account Name</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {profile_content}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
        return (
            <div>
                {user_content}
            </div>
        )
    }
}

Archintel.propTypes = {
    auth: PropTypes.object.isRequired,
    users: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired,
    archintel_accounts: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors,
    archintel_accounts: state.archintel_accounts
});


export default connect(mapStateToProps, null)(Archintel);
