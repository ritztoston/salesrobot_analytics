import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {forgotPassword, unloadMessage, clearErrors} from '../../actions/authActions';
import Pace from 'react-pace-progress';
import TextFieldGroup from '../common/TextFieldGroup';
import {Link, withRouter} from 'react-router-dom';
import classnames from 'classnames';


class ForgotPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: ''
        };

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.props.clearErrors()
    }

    onChange(e) {
        this.setState({[e.target.name]: e.target.value});
    }

    onSubmit(e) {
        e.preventDefault();
        const message = 'Successfully sent recovery link to your Email. Kindly check if you have received the message.';
        const newUser = {
            email: this.state.email
        };

        this.props.forgotPassword(newUser, this.props.errors, this.props.history, message);
    }

    render() {
        const {errors} = this.props;
        const {loading} = this.props.auth;

        return (
            <div>
                {loading ? <Pace color="#FA7921" height={4}/> : null}

                <div className="registration__form">
                    <form action="#" className="form" autoComplete="off" onSubmit={this.onSubmit} encType="application/json">
                        <div className="container">
                            <div className="row u-padding-top-lg">
                                <div className="col">
                                    <div className="form__group">
                                        <div className="u-text-align-center">
                                            <img src="http://localhost:8000/media/default/headernavblue.png" className="header--logo" alt="SalesRobot Logo" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="row u-padding-top-lg-text">
                                <div className="col">
                                    <h1 className="u-heading-xs u-darkgray-text u-text-align-center">Having trouble signing in? Enter your Email to get a recovery link. Once received, you are ask to input a new password.</h1>
                                </div>
                            </div>

                            <div className="row u-padding-top-lg-text">
                                <div className="col">
                                    <TextFieldGroup
                                        placeholder="Enter your Email Address"
                                        placeholder_info="Email"
                                        type="text"
                                        value={this.state.email}
                                        onChange={this.onChange}
                                        error={errors.email}
                                        name="email"
                                    />
                                </div>
                            </div>

                            <div className="row u-padding-top-lg">
                                <div className="col">
                                    <div className="form__group">
                                        <div className="u-text-align-center">
                                            <Link to="/login" className="btn__ btn__cancel">Cancel</Link>
                                            <button className="btn__ btn__submit" type="submit" disabled={loading}>Send Request<div className={classnames('ld ld-spin', {'ld-ring': loading})}/></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

ForgotPassword.propTypes = {
    forgotPassword: PropTypes.func.isRequired,
    unloadMessage: PropTypes.func.isRequired,
    clearErrors: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
});

export default connect(mapStateToProps, {forgotPassword, unloadMessage, clearErrors})(withRouter(ForgotPassword));
