import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {forgotPasswordChecker, unloadMessage, forgotPasswordCP} from '../../actions/authActions';
import Pace from 'react-pace-progress';
import TextFieldGroup from '../common/TextFieldGroup';
import isEmpty from '../../validations/isEmpty';
import {Link, withRouter} from 'react-router-dom';
import classnames from 'classnames';


class ForgotPasswordCP extends Component {
    constructor(props) {
        super(props);
        this.state = {
            password: '',
            password2: '',
            loaded: false
        };

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    componentDidMount() {
        if(this.props.match.params.username) {
            this.props.forgotPasswordChecker(this.props.match.params.username, this.props.match.params.verify_pin)
        }
    }

    static getDerivedStateFromProps(nextProps) {
        if(!isEmpty(nextProps.verification)) {
            return ({loaded: true})
        }
        return null
    }

    onChange(e) {
        this.setState({[e.target.name]: e.target.value});
        console.log(this.props)
    }

    onSubmit(e) {
        e.preventDefault();

        const payload = {
            password: this.state.password,
            password2: this.state.password2
        };
        const message = 'Successfully changed password.';

        this.props.forgotPasswordCP(this.props.match.params.username, this.props.match.params.verify_pin, payload, this.props.errors, this.props.history, message)
    }

    render() {
        const {errors, verification} = this.props;
        const {loading} = this.props.auth;
        const {verify_pin_expires} = this.props.verification;
        const {loaded} = this.state;

        let expired = false;

        const expires = new Date(verify_pin_expires).getTime() / 1000;
        const sec_now = new Date().getTime()/1000;

        if(sec_now >= expires) {
            expired = true
        }

        if(isEmpty(verification)) {
            expired = true
        }

        const expired_content = (
            <div>
                <div className="row u-padding-top-lg">
                    <div className="col">
                        <div className="form__group">
                            <div className="u-text-align-center">
                                <img src="http://localhost:8000/media/default/headernavblue.png" className="header--logo" alt="SalesRobot Logo" />
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row u-padding-top-lg">
                    <div className="col">
                        <h1 className="u-heading-xs u-darkgray-text">Your Forgot password recovery link was <span className="u-error-text">expired</span>. Please issue another request by clicking the button below.</h1>
                    </div>
                </div>

                <div className="row u-padding-top-lg">
                    <div className="col">
                        <Link to="/login" className="btn__ btn__submit">Resend</Link>
                    </div>
                </div>
            </div>
        );

        const not_expired_content = (
            <div>
                <div className="row u-padding-top-lg">
                    <div className="col">
                        <div className="form__group">
                            <div className="u-text-align-center">
                                <img src="http://localhost:8000/media/default/headernavblue.png" className="header--logo" alt="SalesRobot Logo" />
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row u-padding-top-lg-text">
                    <div className="col">
                        <h1 className="u-heading-xs u-darkgray-text u-text-align-center">Please enter your new password to change the forgotten old password.</h1>
                    </div>
                </div>

                <div className="row u-padding-top-lg-text">
                    <div className="col">
                        <TextFieldGroup
                            placeholder="New Password"
                            placeholder_info="New Password"
                            type="password"
                            value={this.state.password}
                            onChange={this.onChange}
                            error={errors.password}
                            name="password"
                        />
                    </div>
                </div>

                <div className="row">
                    <div className="col">
                        <TextFieldGroup
                            placeholder="Confirm Password"
                            placeholder_info="Confirm Password"
                            type="password"
                            value={this.state.password2}
                            onChange={this.onChange}
                            error={errors.password2}
                            name="password2"
                        />
                    </div>
                </div>

                <div className="row u-padding-top-lg">
                    <div className="col">
                        <div className="form__group">
                            <div className="u-text-align-center">
                                <Link to="/login" className="btn__ btn__cancel">Cancel</Link>
                                <button className="btn__ btn__submit" type="submit" disabled={loading}>Change Password<div className={classnames('ld ld-spin', {'ld-ring': loading})}/></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );

        const content = (
            <div className="registration__form">
                <form action="#" className="form" autoComplete="off" onSubmit={this.onSubmit} encType="application/json">
                    <div className="container">
                        {expired ? expired_content : not_expired_content}
                    </div>
                </form>
            </div>
        );

        const unloaded_content = (
            <div>
                {loading ? <Pace color="#FA7921" height={4}/> : content}
            </div>
        );

        const loaded_content = (
            <div>
                {loading ? <Pace color="#FA7921" height={4}/> : null}
                {content}
            </div>
        );

        return (
            <div>
                {loaded ? loaded_content : unloaded_content}
            </div>
        )
    }
}

ForgotPasswordCP.propTypes = {
    forgotPasswordChecker: PropTypes.func.isRequired,
    forgotPasswordCP: PropTypes.func.isRequired,
    unloadMessage: PropTypes.func,
    auth: PropTypes.object.isRequired,
    verification: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors,
    verification: state.verification
});

export default connect(mapStateToProps, {forgotPasswordChecker, unloadMessage, forgotPasswordCP})(withRouter(ForgotPasswordCP));
