from django.urls import path, include
from .views import UserListView, MessageListView, TemplateView
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('subscribers', UserListView)
router.register('template', TemplateView)
router.register('messages', MessageListView, base_name='poc')

urlpatterns = [
    path('', include(router.urls)),
]
