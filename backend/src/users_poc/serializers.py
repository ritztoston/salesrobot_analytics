from .models import PhplistUserUser, PhplistLinktrackUmlClick, PhplistLinktrackForward, PhplistMessage, PhplistUserMessageView, PhplistTemplate
from rest_framework import serializers
from rest_framework.reverse import reverse

from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from django.core.paginator import Paginator

from django.db import connections

class CustomPagination(PageNumberPagination):
    page_size = 15
    page_size_query_param = 'page_size'
    max_page_size = 15
    lookup_value = 'id'

    def get_paginated_response(self, data):
        return Response({
            'next': self.get_next_link(),
            'previous': self.get_previous_link(),
            'count': self.page.paginator.count,
            'results': data
        })

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

class Template(serializers.ModelSerializer):
    class Meta:
        model = PhplistTemplate
        fields = ('template',)

class LinkUrl(serializers.ModelSerializer):
    class Meta:
        model = PhplistLinktrackForward
        fields = ('url', )

class Message(serializers.ModelSerializer):
    class Meta:
        model = PhplistMessage
        fields = ('subject', )

class LinkTrackUMLClick(serializers.HyperlinkedModelSerializer):
    link = serializers.SerializerMethodField(read_only=True)
    message = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = PhplistLinktrackUmlClick
        fields = ('clicked', 'link', 'message', 'firstclick')

    def get_link(self, obj):
        qs = PhplistLinktrackForward.objects.using('poc').filter(id__iexact=obj.get('forwardid', '')).values_list('url')
        return qs[0][0]

    def get_message(self, obj):
        qs = PhplistMessage.objects.using('poc').filter(id__iexact=obj.get('messageid', '')).values_list('subject')
        return qs[0][0]

class UserSerializer(serializers.HyperlinkedModelSerializer):
    linktrackuml = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = PhplistUserUser
        fields = ('id', 'url', 'email', 'confirmed', 'blacklisted', 'bouncecount', 'linktrackuml')

    def get_linktrackuml(self, obj):
        qs = PhplistLinktrackUmlClick.objects.using('poc').filter(userid__iexact=obj.id).values('clicked', 'firstclick', 'forwardid', 'messageid')
        serializer = LinkTrackUMLClick(qs, many=True)
        return serializer.data

class UserSerializerMessage(serializers.HyperlinkedModelSerializer):
    linktrackuml = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = PhplistUserUser
        fields = ('id', 'url', 'email', 'linktrackuml')

    def get_linktrackuml(self, obj):
        qs = PhplistLinktrackUmlClick.objects.using('poc').filter(userid__iexact=obj.id).filter(messageid=self.context.get("messageid")).values('clicked', 'firstclick', 'forwardid', 'messageid')
        serializer = LinkTrackUMLClick(qs, many=True)
        return serializer.data

class MessageListSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='poc-detail')
    unique_views = serializers.SerializerMethodField(read_only=True)
    availurlscount = serializers.SerializerMethodField(read_only=True)
    availurlsclickedcount = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = PhplistMessage
        fields = ('id', 'url', 'subject', 'fromfield', 'sent', 'processed', 'viewed', 'unique_views', 'availurlscount', 'availurlsclickedcount')

    def get_unique_views(self, obj):
        cursor = connections['poc'].cursor()
        cursor.execute("SELECT COUNT(DISTINCT userid) as 'unique_views' FROM phplist_user_message_view WHERE messageid = %s", [obj.id])
        row = dictfetchall(cursor)
        return row[0].get("unique_views")

    def get_availurlscount(self, obj):
        cursor = connections['poc'].cursor()
        cursor.execute("SELECT COUNT(forwardid) as 'availurlscount' FROM phplist_linktrack_ml WHERE messageid = %s", [obj.id])
        row = dictfetchall(cursor)
        return row[0].get("availurlscount")

    def get_availurlsclickedcount(self, obj):
        cursor = connections['poc'].cursor()
        cursor.execute("SELECT SUM(clicked) as 'availurlsclickedcount' FROM phplist_linktrack_ml WHERE messageid = %s", [obj.id])
        row = dictfetchall(cursor)
        return row[0].get("availurlsclickedcount")

class MessageListSerializerDetail(serializers.ModelSerializer):
    users = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = PhplistMessage
        fields = ('id', 'subject', 'fromfield', 'sent', 'processed', 'viewed', 'users')

    def get_users(self, obj):
        cursor = connections['poc'].cursor()
        filter = self.context['request'].GET.get('filter', '')
        if filter == 'all':
            cursor.execute("SELECT phplist_user_message_view.userid,phplist_user_user.email, IF(phplist_linktrack_uml_click.clicked is not NULL, CONCAT('[',GROUP_CONCAT(DISTINCT JSON_OBJECT('clicked',phplist_linktrack_uml_click.clicked,'firstclick',phplist_linktrack_uml_click.firstclick,'url',phplist_linktrack_forward.url)), ']'), null) as 'linktrackuml' FROM phplist_user_message_view LEFT JOIN phplist_message ON phplist_message.id = phplist_user_message_view.messageid LEFT JOIN phplist_user_user ON phplist_user_user.id = phplist_user_message_view.userid LEFT JOIN phplist_linktrack_uml_click ON phplist_linktrack_uml_click.messageid = phplist_user_message_view.messageid AND phplist_linktrack_uml_click.userid = phplist_user_message_view.userid LEFT JOIN phplist_linktrack_forward ON phplist_linktrack_forward.id = phplist_linktrack_uml_click.forwardid WHERE phplist_user_message_view.messageid = %s AND phplist_linktrack_uml_click.clicked is not NULL GROUP BY phplist_user_message_view.userid", [obj.id])
            row = dictfetchall(cursor)
            return row
        elif filter == 'true':
            cursor.execute("SELECT phplist_user_message_view.userid,phplist_user_user.email, IF(phplist_linktrack_uml_click.clicked is not NULL, CONCAT('[',GROUP_CONCAT(DISTINCT JSON_OBJECT('clicked',phplist_linktrack_uml_click.clicked,'firstclick',phplist_linktrack_uml_click.firstclick,'url',phplist_linktrack_forward.url)), ']'), null) as 'linktrackuml' FROM phplist_user_message_view LEFT JOIN phplist_message ON phplist_message.id = phplist_user_message_view.messageid LEFT JOIN phplist_user_user ON phplist_user_user.id = phplist_user_message_view.userid LEFT JOIN phplist_linktrack_uml_click ON phplist_linktrack_uml_click.messageid = phplist_user_message_view.messageid AND phplist_linktrack_uml_click.userid = phplist_user_message_view.userid LEFT JOIN phplist_linktrack_forward ON phplist_linktrack_forward.id = phplist_linktrack_uml_click.forwardid WHERE phplist_user_message_view.messageid = %s AND phplist_linktrack_uml_click.clicked is not NULL GROUP BY phplist_user_message_view.userid", [obj.id])
            row = dictfetchall(cursor)
        else:
            cursor.execute("SELECT phplist_user_message_view.userid,phplist_user_user.email, IF(phplist_linktrack_uml_click.clicked is not NULL, CONCAT('[',GROUP_CONCAT(DISTINCT JSON_OBJECT('clicked',phplist_linktrack_uml_click.clicked,'firstclick',phplist_linktrack_uml_click.firstclick,'url',phplist_linktrack_forward.url)), ']'), null) as 'linktrackuml' FROM phplist_user_message_view LEFT JOIN phplist_message ON phplist_message.id = phplist_user_message_view.messageid LEFT JOIN phplist_user_user ON phplist_user_user.id = phplist_user_message_view.userid LEFT JOIN phplist_linktrack_uml_click ON phplist_linktrack_uml_click.messageid = phplist_user_message_view.messageid AND phplist_linktrack_uml_click.userid = phplist_user_message_view.userid LEFT JOIN phplist_linktrack_forward ON phplist_linktrack_forward.id = phplist_linktrack_uml_click.forwardid WHERE phplist_user_message_view.messageid = %s GROUP BY phplist_user_message_view.userid", [obj.id])
            row = dictfetchall(cursor)

        paginator = CustomPagination()
        paginated_data = paginator.paginate_queryset(queryset=row, request=self.context['request'])
        result = paginator.get_paginated_response(paginated_data)

        return result.data
