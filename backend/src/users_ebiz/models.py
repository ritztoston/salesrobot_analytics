# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class PhplistAdmin(models.Model):
    loginname = models.CharField(unique=True, max_length=25)
    namelc = models.CharField(max_length=255, blank=True, null=True)
    email = models.CharField(max_length=255)
    created = models.DateTimeField(blank=True, null=True)
    modified = models.DateTimeField()
    modifiedby = models.CharField(max_length=25, blank=True, null=True)
    password = models.CharField(max_length=255, blank=True, null=True)
    passwordchanged = models.DateField(blank=True, null=True)
    superuser = models.IntegerField(blank=True, null=True)
    disabled = models.IntegerField(blank=True, null=True)
    privileges = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'phplist_admin'


# class PhplistAdminAttribute(models.Model):
#     adminattributeid = models.IntegerField(primary_key=True)
#     adminid = models.IntegerField()
#     value = models.CharField(max_length=255, blank=True, null=True)
#
#     class Meta:
#         managed = False
#         db_table = 'phplist_admin_attribute'
#         unique_together = (('adminattributeid', 'adminid'),)


class PhplistAdminPasswordRequest(models.Model):
    id_key = models.AutoField(primary_key=True)
    date = models.DateTimeField(blank=True, null=True)
    admin = models.IntegerField(blank=True, null=True)
    key_value = models.CharField(max_length=32)

    class Meta:
        managed = False
        db_table = 'phplist_admin_password_request'


class PhplistAdminattribute(models.Model):
    name = models.CharField(max_length=255)
    type = models.CharField(max_length=30, blank=True, null=True)
    listorder = models.IntegerField(blank=True, null=True)
    default_value = models.CharField(max_length=255, blank=True, null=True)
    required = models.IntegerField(blank=True, null=True)
    tablename = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'phplist_adminattribute'


class PhplistAdmintoken(models.Model):
    adminid = models.IntegerField()
    value = models.CharField(max_length=255, blank=True, null=True)
    entered = models.IntegerField()
    expires = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'phplist_admintoken'


class PhplistAttachment(models.Model):
    filename = models.CharField(max_length=255, blank=True, null=True)
    remotefile = models.CharField(max_length=255, blank=True, null=True)
    mimetype = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    size = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'phplist_attachment'


class PhplistBounce(models.Model):
    date = models.DateTimeField(blank=True, null=True)
    header = models.TextField(blank=True, null=True)
    data = models.TextField(blank=True, null=True)
    status = models.CharField(max_length=255, blank=True, null=True)
    comment = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'phplist_bounce'


class PhplistBounceregex(models.Model):
    regex = models.CharField(unique=True, max_length=255, blank=True, null=True)
    action = models.CharField(max_length=255, blank=True, null=True)
    listorder = models.IntegerField(blank=True, null=True)
    admin = models.IntegerField(blank=True, null=True)
    comment = models.TextField(blank=True, null=True)
    status = models.CharField(max_length=255, blank=True, null=True)
    count = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'phplist_bounceregex'


class PhplistBounceregexBounce(models.Model):
    regex = models.IntegerField(primary_key=True)
    bounce = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'phplist_bounceregex_bounce'
        unique_together = (('regex', 'bounce'),)


class PhplistConfig(models.Model):
    item = models.CharField(primary_key=True, max_length=35)
    value = models.TextField(blank=True, null=True)
    editable = models.IntegerField(blank=True, null=True)
    type = models.CharField(max_length=25, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'phplist_config'


class PhplistEventlog(models.Model):
    entered = models.DateTimeField(blank=True, null=True)
    page = models.CharField(max_length=100, blank=True, null=True)
    entry = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'phplist_eventlog'


class PhplistI18N(models.Model):
    lan = models.CharField(max_length=10)
    original = models.TextField()
    translation = models.TextField()

    class Meta:
        managed = False
        db_table = 'phplist_i18n'
        unique_together = (('lan', 'original'),)


class PhplistLinktrack(models.Model):
    linkid = models.AutoField(primary_key=True)
    messageid = models.IntegerField()
    userid = models.IntegerField()
    url = models.CharField(max_length=255, blank=True, null=True)
    forward = models.TextField(blank=True, null=True)
    firstclick = models.DateTimeField(blank=True, null=True)
    latestclick = models.DateTimeField()
    clicked = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'phplist_linktrack'
        unique_together = (('messageid', 'userid', 'url'),)


class PhplistLinktrackForward(models.Model):
    url = models.CharField(unique=True, max_length=255, blank=True, null=True)
    uuid = models.CharField(max_length=36, blank=True, null=True)
    personalise = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'phplist_linktrack_forward'


class PhplistLinktrackMl(models.Model):
    messageid = models.IntegerField(primary_key=True)
    forwardid = models.IntegerField()
    firstclick = models.DateTimeField(blank=True, null=True)
    latestclick = models.DateTimeField(blank=True, null=True)
    total = models.IntegerField(blank=True, null=True)
    clicked = models.IntegerField(blank=True, null=True)
    htmlclicked = models.IntegerField(blank=True, null=True)
    textclicked = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'phplist_linktrack_ml'
        unique_together = (('messageid', 'forwardid'),)


class PhplistLinktrackUmlClick(models.Model):
    messageid = models.IntegerField()
    userid = models.IntegerField()
    forwardid = models.IntegerField(blank=True, null=True)
    firstclick = models.DateTimeField(blank=True, null=True)
    latestclick = models.DateTimeField(blank=True, null=True)
    clicked = models.IntegerField(blank=True, null=True)
    htmlclicked = models.IntegerField(blank=True, null=True)
    textclicked = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'phplist_linktrack_uml_click'
        unique_together = (('messageid', 'userid', 'forwardid'),)


class PhplistLinktrackUserclick(models.Model):
    linkid = models.IntegerField()
    userid = models.IntegerField()
    messageid = models.IntegerField()
    name = models.CharField(max_length=255, blank=True, null=True)
    data = models.TextField(blank=True, null=True)
    date = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'phplist_linktrack_userclick'


class PhplistList(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    entered = models.DateTimeField(blank=True, null=True)
    listorder = models.IntegerField(blank=True, null=True)
    prefix = models.CharField(max_length=10, blank=True, null=True)
    rssfeed = models.CharField(max_length=255, blank=True, null=True)
    modified = models.DateTimeField()
    active = models.IntegerField(blank=True, null=True)
    owner = models.IntegerField(blank=True, null=True)
    category = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'phplist_list'


class PhplistListmessage(models.Model):
    messageid = models.IntegerField()
    listid = models.IntegerField()
    entered = models.DateTimeField(blank=True, null=True)
    modified = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'phplist_listmessage'
        unique_together = (('messageid', 'listid'),)


class PhplistListuser(models.Model):
    userid = models.IntegerField(primary_key=True)
    listid = models.IntegerField()
    entered = models.DateTimeField(blank=True, null=True)
    modified = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'phplist_listuser'
        unique_together = (('userid', 'listid'),)


class PhplistMessage(models.Model):
    uuid = models.CharField(max_length=36, blank=True, null=True)
    subject = models.CharField(max_length=255)
    fromfield = models.CharField(max_length=255)
    tofield = models.CharField(max_length=255)
    replyto = models.CharField(max_length=255)
    message = models.TextField(blank=True, null=True)
    textmessage = models.TextField(blank=True, null=True)
    footer = models.TextField(blank=True, null=True)
    entered = models.DateTimeField(blank=True, null=True)
    modified = models.DateTimeField()
    embargo = models.DateTimeField(blank=True, null=True)
    repeatinterval = models.IntegerField(blank=True, null=True)
    repeatuntil = models.DateTimeField(blank=True, null=True)
    requeueinterval = models.IntegerField(blank=True, null=True)
    requeueuntil = models.DateTimeField(blank=True, null=True)
    status = models.CharField(max_length=255, blank=True, null=True)
    userselection = models.TextField(blank=True, null=True)
    sent = models.DateTimeField(blank=True, null=True)
    htmlformatted = models.IntegerField(blank=True, null=True)
    sendformat = models.CharField(max_length=20, blank=True, null=True)
    template = models.IntegerField(blank=True, null=True)
    processed = models.PositiveIntegerField(blank=True, null=True)
    astext = models.IntegerField(blank=True, null=True)
    ashtml = models.IntegerField(blank=True, null=True)
    astextandhtml = models.IntegerField(blank=True, null=True)
    aspdf = models.IntegerField(blank=True, null=True)
    astextandpdf = models.IntegerField(blank=True, null=True)
    viewed = models.IntegerField(blank=True, null=True)
    bouncecount = models.IntegerField(blank=True, null=True)
    sendstart = models.DateTimeField(blank=True, null=True)
    rsstemplate = models.CharField(max_length=100, blank=True, null=True)
    owner = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'phplist_message'


class PhplistMessageAttachment(models.Model):
    messageid = models.IntegerField()
    attachmentid = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'phplist_message_attachment'


class PhplistMessagedata(models.Model):
    name = models.CharField(primary_key=True, max_length=100)
    id = models.IntegerField()
    data = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'phplist_messagedata'
        unique_together = (('name', 'id'),)


class PhplistSendprocess(models.Model):
    started = models.DateTimeField(blank=True, null=True)
    modified = models.DateTimeField()
    alive = models.IntegerField(blank=True, null=True)
    ipaddress = models.CharField(max_length=50, blank=True, null=True)
    page = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'phplist_sendprocess'


class PhplistSubscribepage(models.Model):
    title = models.CharField(max_length=255)
    active = models.IntegerField(blank=True, null=True)
    owner = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'phplist_subscribepage'


class PhplistSubscribepageData(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=100)
    data = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'phplist_subscribepage_data'
        unique_together = (('id', 'name'),)


class PhplistTemplate(models.Model):
    title = models.CharField(unique=True, max_length=255)
    template = models.TextField(blank=True, null=True)
    listorder = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'phplist_template'


class PhplistTemplateimage(models.Model):
    template = models.IntegerField()
    mimetype = models.CharField(max_length=100, blank=True, null=True)
    filename = models.CharField(max_length=100, blank=True, null=True)
    data = models.TextField(blank=True, null=True)
    width = models.IntegerField(blank=True, null=True)
    height = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'phplist_templateimage'


class PhplistUrlcache(models.Model):
    url = models.CharField(max_length=255)
    lastmodified = models.IntegerField(blank=True, null=True)
    added = models.DateTimeField(blank=True, null=True)
    content = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'phplist_urlcache'


class PhplistUserAttribute(models.Model):
    name = models.CharField(max_length=255)
    type = models.CharField(max_length=30, blank=True, null=True)
    listorder = models.IntegerField(blank=True, null=True)
    default_value = models.CharField(max_length=255, blank=True, null=True)
    required = models.IntegerField(blank=True, null=True)
    tablename = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'phplist_user_attribute'


class PhplistUserBlacklist(models.Model):
    email = models.CharField(unique=True, max_length=255)
    added = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'phplist_user_blacklist'


class PhplistUserBlacklistData(models.Model):
    email = models.CharField(unique=True, max_length=150)
    name = models.CharField(max_length=25)
    data = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'phplist_user_blacklist_data'


class PhplistUserMessageBounce(models.Model):
    user = models.IntegerField()
    message = models.IntegerField()
    bounce = models.IntegerField()
    time = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'phplist_user_message_bounce'


class PhplistUserMessageForward(models.Model):
    user = models.IntegerField()
    message = models.IntegerField()
    forward = models.CharField(max_length=255, blank=True, null=True)
    status = models.CharField(max_length=255, blank=True, null=True)
    time = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'phplist_user_message_forward'


class PhplistUserMessageView(models.Model):
    messageid = models.IntegerField()
    userid = models.IntegerField()
    viewed = models.DateTimeField(blank=True, null=True)
    ip = models.CharField(max_length=255, blank=True, null=True)
    data = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'phplist_user_message_view'


class PhplistUserUser(models.Model):
    email = models.CharField(unique=True, max_length=255)
    confirmed = models.IntegerField(blank=True, null=True)
    blacklisted = models.IntegerField(blank=True, null=True)
    optedin = models.IntegerField(blank=True, null=True)
    bouncecount = models.IntegerField(blank=True, null=True)
    entered = models.DateTimeField(blank=True, null=True)
    modified = models.DateTimeField()
    uniqid = models.CharField(max_length=255, blank=True, null=True)
    uuid = models.CharField(max_length=36, blank=True, null=True)
    htmlemail = models.IntegerField(blank=True, null=True)
    subscribepage = models.IntegerField(blank=True, null=True)
    rssfrequency = models.CharField(max_length=100, blank=True, null=True)
    password = models.CharField(max_length=255, blank=True, null=True)
    passwordchanged = models.DateField(blank=True, null=True)
    disabled = models.IntegerField(blank=True, null=True)
    extradata = models.TextField(blank=True, null=True)
    foreignkey = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'phplist_user_user'


class PhplistUserUserAttribute(models.Model):
    attributeid = models.IntegerField(primary_key=True)
    userid = models.IntegerField()
    value = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'phplist_user_user_attribute'
        unique_together = (('attributeid', 'userid'),)


class PhplistUserUserHistory(models.Model):
    userid = models.IntegerField()
    ip = models.CharField(max_length=255, blank=True, null=True)
    date = models.DateTimeField(blank=True, null=True)
    summary = models.CharField(max_length=255, blank=True, null=True)
    detail = models.TextField(blank=True, null=True)
    systeminfo = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'phplist_user_user_history'


class PhplistUsermessage(models.Model):
    messageid = models.IntegerField()
    userid = models.IntegerField(primary_key=True)
    entered = models.DateTimeField()
    viewed = models.DateTimeField(blank=True, null=True)
    status = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'phplist_usermessage'
        unique_together = (('userid', 'messageid'),)


class PhplistUserstats(models.Model):
    unixdate = models.IntegerField(blank=True, null=True)
    item = models.CharField(max_length=255, blank=True, null=True)
    listid = models.IntegerField(blank=True, null=True)
    value = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'phplist_userstats'
        unique_together = (('unixdate', 'item', 'listid'),)
