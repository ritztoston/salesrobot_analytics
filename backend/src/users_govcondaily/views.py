from django.core import serializers
from django.shortcuts import render
from rest_framework import generics, mixins, permissions, status, viewsets
from .models import PhplistUserUser, PhplistLinktrackUmlClick, PhplistMessage, PhplistUserMessageView

from .serializers import UserSerializer, LinkTrackUMLClick, MessageListSerializer, MessageListSerializerDetail
from rest_framework.pagination import PageNumberPagination
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework import filters

class StandardResultsSetPagination(PageNumberPagination):
    page_size = 15
    page_size_query_param = 'page_size'
    max_page_size = 15

class UserListView(viewsets.ModelViewSet):
    pagination_class = StandardResultsSetPagination
    serializer_class = UserSerializer
    queryset = PhplistUserUser.objects.using('govcondaily').all()
    permission_classes = []
    http_method_names = ['get']
    filter_backends = (filters.SearchFilter,)
    search_fields = ('email',)

class MessageListView(viewsets.ModelViewSet):
    pagination_class = StandardResultsSetPagination
    queryset = PhplistMessage.objects.using('govcondaily').filter(status__iexact='sent').filter(sent__isnull=False).only('id', 'subject', 'fromfield', 'sent', 'processed', 'viewed')
    permission_classes = []
    http_method_names = ['get']
    filter_backends = (filters.SearchFilter, filters.OrderingFilter,)
    search_fields = ('subject', 'sent',)
    ordering_fields = ('sent',)
    ordering = ('-sent',)

    def get_serializer_class(self):
        if self.action == 'list':
            return MessageListSerializer
        if self.action == 'retrieve':
            return MessageListSerializerDetail
        return serializers.Default
