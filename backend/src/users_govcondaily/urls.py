from django.urls import path, include
from .views import UserListView, MessageListView
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('subscribers', UserListView)
router.register('messages', MessageListView, base_name='govcondaily')

urlpatterns = [
    path('', include(router.urls)),
]
