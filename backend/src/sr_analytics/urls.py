from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
import debug_toolbar

urlpatterns = [
    path('debug/', include(debug_toolbar.urls)),
    path('admin/', admin.site.urls),
    path('api/', include('users.urls')),
    path('poc/', include('users_poc.urls')),
    path('govcondaily/', include('users_govcondaily.urls')),
    path('public/', include('public.urls')),
    path('sosi/', include('users_sosi.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
