from django.urls import path, include
from .views import UserListView, MessageListView, MessageRssItemDateView
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('subscribers', UserListView)
router.register('messages', MessageListView, base_name='sosi')

urlpatterns = [
    path('', include(router.urls)),
    path('messages/rss/<id>/', MessageRssItemDateView.as_view()),
]
