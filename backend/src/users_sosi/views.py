from django.core import serializers
from django.shortcuts import render
from rest_framework import generics, mixins, permissions, status, viewsets, views
from .models import PhplistUserUser, PhplistLinktrackUmlClick, PhplistMessage, PhplistUserMessageView, PhplistList, \
    PhplistListmessage, PhplistRssfeedpluginItemData

from .serializers import UserSerializer, LinkTrackUMLClick, MessageListSerializer, MessageListSerializerDetail, MessageRssItemDateSerializer
from rest_framework.pagination import PageNumberPagination
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework import filters
from django.db.models import Q
from django.db import connections
import ast

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 15
    page_size_query_param = 'page_size'
    max_page_size = 15


class UserListView(viewsets.ModelViewSet):
    pagination_class = StandardResultsSetPagination
    serializer_class = UserSerializer
    queryset = PhplistUserUser.objects.using('sosi').all()
    permission_classes = []
    http_method_names = ['get']
    filter_backends = (filters.SearchFilter,)
    search_fields = ('email',)


class MessageListView(viewsets.ReadOnlyModelViewSet):
    pagination_class = StandardResultsSetPagination
    queryset = PhplistMessage.objects.using('sosi').filter(status__iexact='sent').exclude(sent__isnull=True).only('id',
                                                                                                                  'subject',
                                                                                                                  'fromfield',
                                                                                                                  'sent',
                                                                                                                  'processed',
                                                                                                                  'viewed').filter(
        subject__contains='SOSI Daily Briefing – [TODAY:m/d/Y]').filter(processed__gt=3).order_by('-sent')
    permission_classes = []

    def get_serializer_class(self):
        if self.action == 'list':
            return MessageListSerializer
        if self.action == 'retrieve':
            return MessageListSerializerDetail
        return serializers.Default


class MessageRssItemDateView(views.APIView):
    pagination_class = StandardResultsSetPagination

    def get(self, request, id):
        paginator = StandardResultsSetPagination
        query = "SELECT itemdata.itemid, ml.clicked, (SELECT value FROM phplist_rssfeedplugin_item_data WHERE (property='title') and itemid = item.id) as 'title', fd.url, item.category, (SELECT value FROM phplist_rssfeedplugin_item_data WHERE (property='image') and itemid = item.id) as 'image' FROM phplist_message m LEFT JOIN phplist_messagedata md ON md.id = m.id LEFT JOIN phplist_linktrack_ml ml ON ml.messageid = m.id LEFT JOIN phplist_linktrack_forward fd ON fd.id = ml.forwardid LEFT JOIN phplist_rssfeedplugin_feed feed ON feed.url = md.data LEFT JOIN phplist_rssfeedplugin_item item ON item.feedid = feed.id LEFT JOIN phplist_rssfeedplugin_item_data itemdata ON itemdata.itemid = item.id WHERE m.id = %s AND name='rss_feed' AND item.published = SUBSTRING(m.embargo,1,10) AND (property='url') AND fd.url = itemdata.value GROUP BY fd.id"
        cursor = connections['sosi'].cursor()
        cursor.execute(query, [id])
        row = dictfetchall(cursor)

        serializer = MessageRssItemDateSerializer(row, many=True)

        return Response(serializer.data)
