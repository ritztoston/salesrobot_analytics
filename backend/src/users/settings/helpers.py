import re

# Email
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from lxml import html

# JWT Token
from rest_framework_jwt.settings import api_settings

from sr_analytics.settings import EMAIL_ADMIN, FRONTEND_HTTP_HOST
from users.models import User

# Template
from users.templates.templates import FORGOT_PASSWORD_TEMPLATE1, FORGOT_PASSWORD_TEMPLATE2

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
jwt_response_payload_handler = api_settings.JWT_RESPONSE_PAYLOAD_HANDLER
expires_delta = api_settings.JWT_REFRESH_EXPIRATION_DELTA




def GenerateToken(user):
    payload = jwt_payload_handler(user)
    token = jwt_encode_handler(payload)
    return token


def GeneratePin(length):
    return User.objects.make_random_password(
        length=length,
        allowed_chars='abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ123456789'
    )

def EmailForgotPin(email, verify_pin):
    user = User.objects.filter(email__iexact=email)
    for uname in user:
        username = uname.username
    link = FRONTEND_HTTP_HOST + 'forgot-password/' + username + '/' + verify_pin + '/'
    subject, from_email = 'Forgot Password Pin', EMAIL_ADMIN
    to_email = email
    html_content = FORGOT_PASSWORD_TEMPLATE1 + FORGOT_PASSWORD_TEMPLATE2.format(
        code_link=link, code=verify_pin)
    doc = html.fromstring(html_content)
    text_content = doc.text_content()
    msg = EmailMultiAlternatives(
        subject, text_content, from_email, [to_email])
    msg.attach_alternative(html_content, "text/html")
    msg.send()

def letters_and_num_only(value):
    if re.match("^[A-Za-z0-9_-]*$", value):
        return True
    return False


def letters_only(value):
    if re.match("^[A-Za-z]*$", value):
        return True
    return False
