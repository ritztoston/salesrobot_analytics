from django.utils import timezone
from django.conf import settings
from datetime import datetime
import time
from calendar import timegm
from rest_framework_jwt.settings import api_settings

expires_delta = api_settings.JWT_REFRESH_EXPIRATION_DELTA

def jwt_payload_handler(user):
    return {
        'user_id': user.id,
        'email': user.email,
        'is_superuser': user.is_superuser,
        'staff_user': user.staff_user,
        'username': user.username,
        'firstname': user.firstname,
        'lastname': user.lastname,
        'avatar': user.avatar.url,
        'exp': datetime.utcnow() + api_settings.JWT_EXPIRATION_DELTA,
        'orig_iat': timegm(datetime.utcnow().utctimetuple())
    }

def jwt_response_payload_handler(token, user=None, request=None):
    return {
        'token': token,
        'user': user.id,
      	'expires': timezone.now() + expires_delta
    }
