# Generated by Django 2.1.3 on 2018-12-13 12:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0011_auto_20181213_2041'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='jwt_issue_dt',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
