import os, time
from datetime import datetime
from django.utils import timezone
from calendar import timegm
from rest_framework import serializers
from rest_framework.response import Response

from .models import User

from .settings.helpers import letters_and_num_only, letters_only, GeneratePin, EmailForgotPin

from rest_framework_jwt.settings import api_settings
from .settings import helpers
import jwt

expires_delta = api_settings.JWT_REFRESH_EXPIRATION_DELTA


class UserSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'url', 'username', 'email', 'firstname', 'lastname', 'avatar', 'is_superuser', 'staff_user')

class UserUpdateSerializer(serializers.HyperlinkedModelSerializer):
    token = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = User
        fields = ('id', 'url', 'username', 'email', 'firstname', 'lastname', 'avatar', 'token')

    def get_token(self, obj):
        user = User.objects.get(username__iexact=self.instance)
        return helpers.GenerateToken(user)

    def validate_username(self, value):
        if not letters_only(value):
            raise serializers.ValidationError('Only letters are allowed.')
        if len(value) == 0 or value is None or value == '':
            raise serializers.ValidationError('Username is required.')
        if len(value) < 5 or len(value) > 11:
            raise serializers.ValidationError(
                'Minimum of 5 or maximum of 11 characters only.')
        return value

    def validate_email(self, value):
        if value == '':
            raise serializers.ValidationError('Email is required.')
        return value

    def validate_firstname(self, value):
        if not letters_only(value):
            raise serializers.ValidationError('Only letters are allowed')
        return value

    def validate_lastname(self, value):
        if not letters_only(value):
            raise serializers.ValidationError('Only letters are allowed')
        return value

    def update(self, instance, validated_data):

        if instance.username != validated_data.get('username'):
            instance.username = validated_data.get('username')

        if instance.email != validated_data.get('email'):
            instance.email = validated_data.get('email')

        if instance.firstname != validated_data.get('firstname'):
            instance.firstname = validated_data.get('firstname')

        if instance.lastname != validated_data.get('lastname'):
            instance.lastname = validated_data.get('lastname')

        if validated_data.get('avatar'):
            if instance.avatar != validated_data.get('avatar'):
                o_avatar = instance.avatar
                instance.avatar = validated_data.get('avatar')
                if not instance.avatar:
                    instance.avatar = o_avatar

        instance.save()
        return instance

class UserRegisterSerializer(serializers.HyperlinkedModelSerializer):
    staff_choices = (
        (0, 'ArchIntel User'),
        (1, 'ArchIntel Admin User'),
        (2, 'SOSi User'),
        (3, 'SOSi Admin User')
    )
    password = serializers.CharField(style={'input_type': 'password'}, max_length=255, write_only=True, allow_null=True, allow_blank=True)
    password2 = serializers.CharField(style={'input_type': 'password'}, max_length=255, write_only=True, allow_null=True, allow_blank=True)
    staff_user = serializers.ChoiceField(choices=staff_choices)

    class Meta:
        model = User
        fields = ('id', 'url', 'username', 'email', 'firstname', 'lastname', 'avatar', 'staff_user', 'password', 'password2')

    def validate(self, data):
        pw = data.get('password')
        pw2 = data.pop('password2')
        if pw == '':
            raise serializers.ValidationError({'password': 'Password is required.'})
        if pw != pw2:
            raise serializers.ValidationError({'password2': 'Password must match.'})

        return data

    def validate_username(self, value):
        if not letters_only(value):
            raise serializers.ValidationError('Only letters are allowed.')
        if len(value) == 0 or value is None or value == '':
            raise serializers.ValidationError('Username is required.')
        if len(value) < 5 or len(value) > 11:
            raise serializers.ValidationError('Minimum of 5 or maximum of 11 characters only.')
        return value

    def validate_email(self, value):
        if value == '':
            raise serializers.ValidationError('Email is required.')
        return value

    def validate_firstname(self, value):
        if not letters_only(value):
            raise serializers.ValidationError('Only letters are allowed')
        if len(value) == 0 or value is None or value == '':
            raise serializers.ValidationError('First Name is required.')
        return value

    def validate_lastname(self, value):
        if not letters_only(value):
            raise serializers.ValidationError('Only letters are allowed')
        if len(value) == 0 or value is None or value == '':
            raise serializers.ValidationError('Last Name is required.')
        return value

    def validate_password(self, value):
        if len(value) == 0 or value is None or value == '':
            raise serializers.ValidationError('Password is required.')
        if len(value) < 4 or len(value) > 11:
            raise serializers.ValidationError('Minimum of 4 or maximum of 11 characters only.')
        return value

    def validate_password2(self, value):
        if len(value) == 0 or value is None or value == '':
            raise serializers.ValidationError('Confirm password is required.')
        return value

    def create(self, validated_data):
        avatar = ''
        if validated_data.get('avatar') == None:
            avatar = 'default/' + os.path.basename(os.path.join(os.path.dirname(os.path.dirname(os.getcwd())), 'media', 'default', 'no-image.png'))
        else:
            avatar = validated_data['avatar']

        user = User(
            email = validated_data['email'],
            username = validated_data['username'],
            firstname = validated_data['firstname'],
            lastname = validated_data['lastname'],
            staff_user = validated_data['staff_user'],
            avatar = avatar
        )

        user.set_password(validated_data['password'])
        user.save()

        return user

class UserLoginSerializer(serializers.HyperlinkedModelSerializer):
    token = serializers.SerializerMethodField(read_only=True)
    username = serializers.CharField(required=True)
    password = serializers.CharField(style={'input_type': 'password'}, required=True, write_only=True)

    class Meta:
        model = User
        fields = ('id', 'url', 'username', 'email', 'firstname', 'lastname', 'avatar', 'password', 'token')
        read_only_fields = ('email', 'firstname', 'lastname', 'avatar')

    def get_token(self, obj):
        return obj.token

    def validate_username(self, data):
        user = User.objects.filter(username__iexact=data)
        if user.exists():
            return data

        raise serializers.ValidationError('Username does not exists.')

    def validate(self, data):
        username = data.get('username')
        password = data.get('password')
        user = User.objects.get(username__iexact=username)
        if not user.check_password(password):
            raise serializers.ValidationError({'password': 'Invalid password. Please try again.'})

        return data

class ChangePasswordSerializer(serializers.ModelSerializer):
    password = serializers.CharField(style={'input_type': 'password'}, required=True, write_only=True)
    password1 = serializers.CharField(style={'input_type': 'password'}, required=True, write_only=True)
    password2 = serializers.CharField(style={'input_type': 'password'}, required=True, write_only=True)

    url = serializers.HyperlinkedIdentityField(view_name='change-password-detail')

    class Meta:
        model = User
        fields = ('id', 'url', 'username', 'password', 'password1', 'password2')

    def validate(self, data):
        currentpw = data.get('password')
        pw = data.get('password1')
        pw2 = data.pop('password2')
        id = self.instance.id
        user = User.objects.get(id__iexact=id)
        if not user.check_password(currentpw):
            raise serializers.ValidationError({'password': 'Invalid password. Please try again.'})
        if pw != pw2:
            raise serializers.ValidationError({'password2':'Passwords must match.'})
        if currentpw == pw:
            raise serializers.ValidationError({'password1': 'Can\'t change password which is similar with your current password.'})
        return data

    def update(self, instance, validated_data):
        instance.set_password(validated_data.get('password1'))
        instance.save()
        return instance

class AdminRegisterSerializer(serializers.HyperlinkedModelSerializer):
    staff_choices = (
        (0, 'ArchIntel User'),
        (1, 'ArchIntel Admin User'),
        (2, 'SOSi User'),
        (3, 'SOSi Admin User')
    )
    staff_user = serializers.ChoiceField(choices=staff_choices)
    password = serializers.CharField(style={'input_type': 'password'}, max_length=255, write_only=True, allow_null=True, allow_blank=True)
    password2 = serializers.CharField(style={'input_type': 'password'}, max_length=255, write_only=True, allow_null=True, allow_blank=True)

    class Meta:
        model = User
        fields = ('id', 'url', 'username', 'email', 'firstname',
                  'lastname', 'avatar', 'staff_user', 'password', 'password2')

    def validate(self, data):
        pw = data.get('password')
        pw2 = data.pop('password2')
        if pw == '':
            raise serializers.ValidationError(
                {'password': 'Password is required.'})
        if pw != pw2:
            raise serializers.ValidationError(
                {'password2': 'Password must match.'})

        return data

    def validate_username(self, value):
        if not letters_only(value):
            raise serializers.ValidationError('Only letters are allowed.')
        if len(value) == 0 or value is None or value == '':
            raise serializers.ValidationError('Username is required.')
        if len(value) < 5 or len(value) > 11:
            raise serializers.ValidationError(
                'Minimum of 5 or maximum of 11 characters only.')
        return value

    def validate_email(self, value):
        if value == '':
            raise serializers.ValidationError('Email is required.')
        return value

    def validate_firstname(self, value):
        if not letters_only(value):
            raise serializers.ValidationError('Only letters are allowed')
        if len(value) == 0 or value is None or value == '':
            raise serializers.ValidationError('First Name is required.')
        return value

    def validate_lastname(self, value):
        if not letters_only(value):
            raise serializers.ValidationError('Only letters are allowed')
        if len(value) == 0 or value is None or value == '':
            raise serializers.ValidationError('Last Name is required.')
        return value

    def validate_password(self, value):
        if len(value) == 0 or value is None or value == '':
            raise serializers.ValidationError('Password is required.')
        if len(value) < 4 or len(value) > 11:
            raise serializers.ValidationError(
                'Minimum of 4 or maximum of 11 characters only.')
        return value

    def validate_password2(self, value):
        if len(value) == 0 or value is None or value == '':
            raise serializers.ValidationError('Confirm password is required.')
        return value

    def create(self, validated_data):
        avatar = ''
        if validated_data.get('avatar') == None:
            avatar = 'default/' + os.path.basename(os.path.join(os.path.dirname(
                os.path.dirname(os.getcwd())), 'media', 'default', 'no-image.png'))
        else:
            avatar = validated_data['avatar']

        user = User(
            email=validated_data['email'],
            username=validated_data['username'],
            firstname=validated_data['firstname'],
            lastname=validated_data['lastname'],
            staff_user=validated_data['staff_user'],
            avatar=avatar,
            is_superuser=True
        )

        user.set_password(validated_data['password'])
        user.save()

        return user

class ForgotPasswordSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(required=True).default_error_messages['blank'] = 'Email is required.'

    class Meta:
        model = User
        fields = (
            'id',
			'firstname',
			'lastname',
			'email'
		)
        read_only_fields = ('firstname', 'lastname')

    def validate_email(self, data):
        if len(data) == 0 or data is None or data == '':
            raise serializers.ValidationError('Email is required.')

        user = User.objects.filter(email__iexact=data)
        if user.exists():
            return data
        raise serializers.ValidationError('Email not found.')

    def update(self, instance, validated_data):
        instance.verify_pin = GeneratePin(11)
        instance.verify_pin_expires = timezone.now() + api_settings.JWT_EXPIRATION_DELTA
        instance.save()
        EmailForgotPin(instance.email, instance.verify_pin)
        return instance

class VerifyEmailSerializer(serializers.ModelSerializer):
    password = serializers.CharField(style={'input_type': 'password'}, required=True, write_only=True)
    password2 = serializers.CharField(style={'input_type': 'password'}, required=True, write_only=True)

    class Meta:
        model = User
        fields = (
            'id',
			'username',
			'firstname',
			'lastname',
			'email',
			'verify_pin',
			'verify_pin_expires',
            'password',
            'password2',
		)
        read_only_fields = ('username', 'email', 'verify_pin', 'verify_pin_expires', 'firstname', 'lastname')

    def validate(self, data):
        expires = self.instance.verify_pin_expires
        date_now = timezone.now()
        print(date_now)
        print(expires)
        if not self.instance.verify_pin:
            raise serializers.ValidationError({'errors': 'Forgot password verfication pin was expired.'})
        if date_now >= expires:
            raise serializers.ValidationError({'errors': 'Forgot password verfication pin was expired.'})
        currentpw = data.get('password')
        pw = data.get('password')
        pw2 = data.pop('password2')
        id = self.instance.id
        user = User.objects.get(id__iexact=id)
        if user.check_password(pw):
            raise serializers.ValidationError({'password': 'Can\'t change password which is similar with your current password.'})
        if pw != pw2:
            raise serializers.ValidationError({'password2':'Passwords must match.'})
        return data

    def update(self, instance, validated_data):
        instance.verify_pin = None
        instance.verify_pin_expires = None
        instance.set_password(validated_data.get('password'))
        instance.save()

        return instance
