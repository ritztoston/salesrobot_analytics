from django.test import TestCase
# from sr_analytics.settings import EMAIL_HOST_USER

# Create your tests here.
from django.core.mail import EmailMultiAlternatives

subject, from_email, to = 'hello', 'media@salesrobot.com', 'kim.toston@executivemosaic.com'
text_content = 'This is an important message.'
html_content = '<p>This is an <strong>important</strong> message.</p>'
msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
msg.attach_alternative(html_content, "text/html")
msg.send()
