from django.core import serializers
from django.shortcuts import render
from django.http import Http404
from rest_framework import generics, mixins, permissions, status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.exceptions import APIException

from .models import User
from .settings import helpers

from . import permissions as permission
from .serializers import (AdminRegisterSerializer, ChangePasswordSerializer,
                          UserLoginSerializer, UserRegisterSerializer,
                          UserSerializer, ForgotPasswordSerializer, VerifyEmailSerializer, UserUpdateSerializer)

class UpdateSerializerMixin(object):
    "specify a different serializer to use for PUT requests"

    def get_serializer_class(self):
        "return a different serializer if performing an update"
        serializer_class = self.serializer_class

        if self.request.method == 'PUT' and self.update_serializer_class:
            serializer_class = self.update_serializer_class

        return serializer_class

class UserListView(UpdateSerializerMixin, viewsets.ModelViewSet):
    serializer_class = UserSerializer
    update_serializer_class = UserUpdateSerializer
    queryset = User.objects.all()
    permission_classes = [permission.OwnOrAdmin]
    http_method_names = ['get', 'delete', 'put', 'patch']

class UserAuthView(mixins.ListModelMixin, viewsets.GenericViewSet):
    queryset = User.objects.all()

    def list(self, request):
        loaders = {
            'Register': reverse('auth-register', request=request),
            'Register Admin': reverse('auth-register-admin', request=request),
            'Login': reverse('auth-login', request=request),
            'Forgot Password': reverse('auth-forgot-password', request=request),
        }
        return Response(loaders)

    @action(
        detail=False,
        methods=['post'],
        permission_classes = [],
        serializer_class = UserRegisterSerializer,
    )
    def register(self, request, pk=None):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer = self.serializer_class(data=request.data, context={'request': request})
            serializer.is_valid()
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(
        detail=False,
        methods=['post'],
        permission_classes=[permissions.IsAdminUser],
        serializer_class=AdminRegisterSerializer,
    )
    def register_admin(self, request, pk=None):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer = self.serializer_class(
                data=request.data, context={'request': request})
            serializer.is_valid()
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(
        detail=False,
        methods=['post'],
        permission_classes = [permissions.AllowAny],
        serializer_class = UserLoginSerializer,
    )
    def login(self, request, pk=None):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            user = User.objects.get(username__iexact=request.data.get('username'))
            user.token = helpers.GenerateToken(user)
            serializer = self.serializer_class(user, context={'request': request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(
        detail=False,
        methods=['put'],
        permission_classes=[permissions.AllowAny],
        serializer_class = ForgotPasswordSerializer,
    )
    def forgot_password(self, request, pk=None):
        try:
            user = User.objects.get(email__iexact=request.data.get('email'))
            serializer = self.serializer_class(user, data=request.data,context={'request': request})
        except:
            serializer = self.serializer_class(data=request.data,context={'request': request})

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ChangePasswordView(viewsets.ModelViewSet):
    serializer_class = ChangePasswordSerializer
    queryset = User.objects.all()
    permission_classes = [permission.OwnOrAdmin]
    http_method_names = ['put', 'get']

class ForgotPasswordView(viewsets.ModelViewSet):
    serializer_class = VerifyEmailSerializer
    permission_classes = [permissions.AllowAny]
    http_method_names = ['put', 'get']
    lookup_field = 'username'

    def get_queryset(self):
        username = self.kwargs['username']
        verify_pin = self.kwargs['verify_pin']
        try:
            queryset = User.objects.filter(username__iexact=username).filter(verify_pin__iexact=verify_pin)
        except:
            raise APIException({'email': 'Email not found.'})
        if not queryset:
            raise APIException({'errors': 'Forgot password verfication pin was expired.'})
        return queryset
