from django.urls import path, include
from .views import UserListView, UserAuthView, ChangePasswordView, ForgotPasswordView
from rest_framework.routers import DefaultRouter
from rest_framework_jwt.views import refresh_jwt_token

router = DefaultRouter()
router.register('users', UserListView)
router.register('auth', UserAuthView, base_name='auth')
router.register('auth/change-password', ChangePasswordView, base_name='change-password')

urlpatterns = [
    path('', include(router.urls)),
    path('auth/forgot-password/<username>/<verify_pin>/', ForgotPasswordView.as_view({'get': 'list', 'put': 'update'})),
    path('auth/refresh-token/', refresh_jwt_token)
]
