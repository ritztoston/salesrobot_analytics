from .models import PhplistUserUser
from rest_framework import serializers
from rest_framework.reverse import reverse

from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from django.core.paginator import Paginator
from rest_framework import filters, views
from django.db import connections

class UsersSerializers(serializers.ModelSerializer):

    class Meta:
        model = PhplistUserUser
        fields = ('id', 'email')

class AllClicksUserSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    linktrackuml = serializers.JSONField()
    email = serializers.CharField()

class UsersSerializersCustom(serializers.Serializer):
    id = serializers.IntegerField()
    email = serializers.CharField()
    count = serializers.IntegerField()
