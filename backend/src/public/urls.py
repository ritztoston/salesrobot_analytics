from django.urls import path, include
from .views import AllUsersView, AllUserClicksDetailView
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('subscribers', AllUsersView, base_name='subscribers')

urlpatterns = [
    path('', include(router.urls)),
    path('subscribers/all/<email>/', AllUserClicksDetailView.as_view()),
]
