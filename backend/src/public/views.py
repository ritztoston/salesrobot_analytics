from django.core import serializers
from django.shortcuts import render
from rest_framework import generics, mixins, permissions, status, viewsets, filters, views
from .models import PhplistUserUser

from .serializers import UsersSerializers, AllClicksUserSerializer, UsersSerializersCustom
from rest_framework.pagination import PageNumberPagination
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.reverse import reverse
from django.db import connections
from itertools import chain
import ast
from operator import itemgetter


def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 15
    page_size_query_param = 'page_size'
    max_page_size = 15


class UserListView(viewsets.ModelViewSet):
    pagination_class = StandardResultsSetPagination
    serializer_class = UsersSerializers
    queryset = PhplistUserUser.objects.using('poc').all()
    permission_classes = []
    # http_method_names = ['get']
    filter_backends = (filters.SearchFilter,)
    search_fields = ('email',)


class AllUserClicksDetailView(views.APIView):

    def get(self, request, email):
        query = "SELECT phplist_user_user.id, phplist_user_user.email, IF(phplist_linktrack_uml_click.messageid IS NOT NULL, CONCAT('[', GROUP_CONCAT( DISTINCT JSON_OBJECT( 'id',phplist_linktrack_uml_click.id, 'url',phplist_linktrack_forward.url, 'clicked',phplist_linktrack_uml_click.firstclick, 'clicks',phplist_linktrack_uml_click.clicked)), ']'), NULL) AS 'linktrackuml' FROM phplist_user_user LEFT JOIN phplist_linktrack_uml_click ON phplist_user_user.id = phplist_linktrack_uml_click.userid LEFT JOIN phplist_linktrack_forward ON phplist_linktrack_forward.id = phplist_linktrack_uml_click.forwardid WHERE phplist_user_user.email = %s"
        cursor = connections['poc'].cursor()
        cursor2 = connections['govcondaily'].cursor()

        cursor.execute(query, [email])
        cursor2.execute(query, [email])

        row = dictfetchall(cursor)
        row2 = dictfetchall(cursor2)

        try:
            row[0]['linktrackuml'] = ast.literal_eval(row[0]['linktrackuml'])
        except:
            pass

        try:
            row2[0]['linktrackuml'] = ast.literal_eval(row2[0]['linktrackuml'])
        except:
            pass

        if row[0]['linktrackuml'] is not None and row2[0]['linktrackuml'] is not None:
            row[0]['linktrackuml'] = list(chain(row[0]['linktrackuml'], row2[0]['linktrackuml']))
        elif row[0]['linktrackuml'] is None and row2[0]['linktrackuml'] is not None:
            row[0]['linktrackuml'] = row2[0]['linktrackuml']
        elif row[0]['linktrackuml'] is not None and row2[0]['linktrackuml'] is None:
            row[0]['linktrackuml'] = row[0]['linktrackuml']

        res_list = []
        if row[0]['linktrackuml'] is not None:
            for i in range(len(row[0]['linktrackuml'])):
                if row[0]['linktrackuml'][i] not in row[0]['linktrackuml'][i + 1:]:
                    res_list.append(row[0]['linktrackuml'][i])

        newlist = sorted(res_list, key=itemgetter('clicks'), reverse=True)
        row[0]['linktrackuml'] = newlist

        serializer = AllClicksUserSerializer(row, many=True)

        return Response(serializer.data)


class AllUsersView(viewsets.ReadOnlyModelViewSet):
    pagination_class = StandardResultsSetPagination
    serializer_class = UsersSerializersCustom

    def get_queryset(self):
        filter = self.request.GET.get('email', '') + '%'

        if filter and filter != '%':
            query = "SELECT phplist_user_user.id, phplist_user_user.email, COUNT(phplist_linktrack_uml_click.userid) AS 'count' FROM phplist_user_user LEFT JOIN phplist_linktrack_uml_click ON phplist_user_user.id = phplist_linktrack_uml_click.userid WHERE email LIKE %s GROUP BY phplist_user_user.email ORDER BY phplist_user_user.email ASC"
            cursor = connections['poc'].cursor()
            cursor2 = connections['govcondaily'].cursor()
            cursor.execute(query, [filter])
            cursor2.execute(query, [filter])

            row = dictfetchall(cursor)
            row2 = dictfetchall(cursor2)

        else:
            query = "SELECT phplist_user_user.id, phplist_user_user.email, COUNT(phplist_linktrack_uml_click.userid) AS 'count' FROM phplist_user_user LEFT JOIN phplist_linktrack_uml_click ON phplist_user_user.id = phplist_linktrack_uml_click.userid GROUP BY phplist_user_user.email ORDER BY phplist_user_user.email ASC"
            cursor = connections['poc'].cursor()
            cursor2 = connections['govcondaily'].cursor()
            cursor.execute(query)
            cursor2.execute(query)

            row = dictfetchall(cursor)
            row2 = dictfetchall(cursor2)

        chained_list = list(chain(row, row2))

        new = {}

        for d in chained_list:
            name = d["email"]
            if name in new:
                new[name]["count"] += d["count"]
            else:
                new[name] = dict(d)

        result = list(new.values())
        newlist = sorted(result, key=itemgetter('count'), reverse=True)
        return newlist
